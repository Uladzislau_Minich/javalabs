package com.epam.newsclient.command.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsclient.command.ICommand;
import com.epam.newsclient.utils.ConfigurationManager;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.entity.NewsValueObject;
import com.epam.newscommon.entity.Tag;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsValueObjectService;
import com.epam.newscommon.utils.SearchCriteriaObject;

/**
 * Class ShowListNewsCommand.
 * 
 * @author Uladzislau
 *
 */
public class ShowListNewsCommand implements ICommand {

	@Autowired
	private NewsValueObjectService service;

	private static final int COUNT_NEWS_ON_PAGE = 3;
	private static final String AUTHORS = "authors";
	private static final String TAGS = "tags";
	private static final String INDEX_PAGE = "indexNewsPage";
	private static final String NEWS_LIST = "newsList";
	private static final String COUNT_NEWS = "countNews";
	private static final String PATH_PAGE_SUCCESS = "path.page.newslist";
	private static final String PATH_PAGE_ERROR = "path.page.error";
	private static final String ATTR_SESSION_AUTHOR = "authorSelect";
	private static final String ATTR_SESSION_TAGS = "tagsSelect";
	private final static String ID_PREVIOUS_PAGE = "idPreviousNewsPage";
	private static final String PAR_RESET_FILTER = "resetFilter";
	private static final String ATTR_SESSION_SEARCH_CRITERIA = "searchCriteria";

	/**
	 * Show news's list by page
	 */
	public String execute(HttpServletRequest request) throws ServiceException {
		String authorSearch = request.getParameter(ATTR_SESSION_AUTHOR);
		String[] tagsSearch = request.getParameterValues(ATTR_SESSION_TAGS);
		int indexPageNews = Integer.valueOf(request.getParameter(INDEX_PAGE));
		String resetFilter = request.getParameter(PAR_RESET_FILTER);
		List<NewsValueObject> news = new ArrayList<>();
		SearchCriteriaObject sc = (SearchCriteriaObject) request.getSession()
				.getAttribute(ATTR_SESSION_SEARCH_CRITERIA);
		if (resetFilter != null && resetFilter.equals("true")) {
			request.getSession().setAttribute(ATTR_SESSION_SEARCH_CRITERIA, null);
			sc = null;
		}
		if (tagsSearch != null || authorSearch != null) {
			sc = createSearchCriteria(authorSearch, tagsSearch);
		}
		int startIndex = (indexPageNews - 1) * COUNT_NEWS_ON_PAGE + 1;
		int endIndex = indexPageNews * COUNT_NEWS_ON_PAGE;
		news = service.findNewsPerPage(sc, startIndex, endIndex);
		request.setAttribute(COUNT_NEWS, service.countNews(sc));
		request.setAttribute(INDEX_PAGE, indexPageNews);
		request.getSession().setAttribute(ATTR_SESSION_SEARCH_CRITERIA, sc);
		List<Author> authors = service.findAllAuthors();
		List<Tag> tags = service.findAllTags();
		if (authors.isEmpty() || authors == null || tags.isEmpty() || tags == null) {
			return ConfigurationManager.getProperty(PATH_PAGE_ERROR);
		}
		request.getSession().setAttribute(ID_PREVIOUS_PAGE, indexPageNews);
		request.setAttribute(AUTHORS, authors);
		request.setAttribute(TAGS, tags);
		request.setAttribute(NEWS_LIST, news);
		return ConfigurationManager.getProperty(PATH_PAGE_SUCCESS);
	}

	private SearchCriteriaObject createSearchCriteria(String authorId, String[] tagIds) {
		SearchCriteriaObject sc = new SearchCriteriaObject();
		if (authorId != null) {
			sc.setAuthorId(Long.parseLong(authorId));
		}
		if (tagIds != null) {
			List<Long> tagIdsList = new ArrayList<>();
			for (String id : tagIds) {
				tagIdsList.add(Long.parseLong(id));
			}
			sc.getTagIds(tagIdsList);
		}
		return sc;
	}
}
