package com.epam.news.service.implementation;

import java.util.List;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteriaObject;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;

/**
 * Class NewsServiceImpl. Implementation of the NewsService
 * 
 * @author Uladzislau_Minich
 *
 */
public class NewsServiceImpl implements NewsService {
	private NewsDAO newsDAO;

	/**
	 * @see {@link com.epam.news.service.NewsService#save()}
	 */
	@Override
	public Long save(News entity) throws ServiceException {
		try {
			entity.setNewsId(newsDAO.create(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity.getNewsId();
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#findById()}
	 */
	@Override
	public News findById(Long id) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.findById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#update()}
	 */
	@Override
	public boolean update(News entity) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#deleteById(int)}
	 */
	@Override
	public boolean deleteById(Long id) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#findAllNews()}
	 */
	@Override
	public List<News> findAllNews() throws ServiceException {
		List<News> newsAll = null;
		try {
			newsAll = newsDAO.findAllNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsAll;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#findNewsByTitle(String)}
	 */
	@Override
	public News findNewsByTitle(String title) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.findNewsByTitle(title);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#findNewsBySearchCriteria(SearchCriteriaObject)}
	 */
	@Override
	public List<News> findNewsBySearchCriteria(SearchCriteriaObject sc) throws ServiceException {
		List<News> searchNews = null;
		try {
			searchNews = newsDAO.findNewsBySearchCriteria(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return searchNews;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#saveNewsAuthors(int, int)}
	 */
	@Override
	public boolean saveNewsAuthors(Long newsId, Long authorId) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.saveNewsAuthors(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#deleteNewsAuthor(int)}
	 */
	@Override
	public boolean deleteNewsAuthor(Long newsId) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.deleteNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#addNewsTag(int, int)}
	 */
	@Override
	public boolean addNewsTag(Long tagId, Long newsId) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.addNewsTag(tagId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#deleteNewsTag(int)}
	 */
	@Override
	public boolean deleteNewsTag(Long idNews) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsDAO.deleteNewsTag(idNews);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * @see {@link com.epam.news.service.NewsService#findAllAuthorNews(int)}
	 */
	@Override
	public List<News> findAllAuthorNews(Long authorId) throws ServiceException {
		List<News> searchNews = null;
		try {
			searchNews = newsDAO.findAllAuthorsNews(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return searchNews;
	}
	/**
	 * @see {@link com.epam.news.service.NewsService#countNews()}
	 */
	@Override
	public int countNews() throws ServiceException {
		int amount = 0;
		try {
			 amount = newsDAO.countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return amount;
	}
	/**
	 * Set  newsDAO
	 * 
	 * @param newsDAO
	 */
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	

}
