public class LoadingClassException {
	
	/* ExceptionInInitializerError */

	// static int i = 10/0; arithmetic exception
	
	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		/* ClassNotFoundException */

		// Class classs = Class.forName("notExistingClass"); exception here
		// Object obj = classs.newInstance();

		/* ClassCastException */

		// String str = new String("ad");
		// Integer i = new Integer(1);
		// Object strs = i;
		// str = (String) strs; exception here

		/* UnsatisfiedLinkError */

		// LoadingClass.method(); - can't find definition of native method

	}

	public static native void method();
}
