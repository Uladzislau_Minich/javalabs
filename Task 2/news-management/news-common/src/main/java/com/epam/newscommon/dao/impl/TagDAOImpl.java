package com.epam.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newscommon.constant.DBColumnName;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.dao.TagDAO;
import com.epam.newscommon.entity.Tag;

/**
 * Class TagDAOImpl. Implementation of TagDAO
 * 
 * @author Uladzislau_Minich
 *
 */
public class TagDAOImpl implements TagDAO {
	private BasicDataSource dataSource;
	public static final String SQL_CREATE_TAG = "Insert into tag(tag_id,tag_name) values(TAG_SEQ.nextval,?)";
	public static final String SQL_UPDATE_TAG = "Update tag SET tag_name=? where tag_id=?";
	public static final String SQL_DELETE_TAG = "Delete from tag where tag_id=?";
	public static final String SQL_DELETE_TAGS_FROM_NEWS_TAGS = "Delete from news_tag where tag_id=?";
	public static final String SQL_FIND_BY_ID = "Select * from tag where tag_id=?";
	public static final String SQL_FIND_TAGS_BY_NEWS_ID = "Select * from TAG  JOIN news_tag ON (news_tag.tag_id=tag.tag_id) where news_tag.news_id=?";
	public static final String SQL_FIND_ALL_TAGS = "Select * from TAG";
	public static final String SQL_FIND_MAX_PK = "Select MAX(tag_id) from tag";

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#create(Tag)}
	 */
	@Override
	public Long create(Tag entity) throws DAOException {
		Long id = (long) 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_TAG,
				new String[] { DBColumnName.TAG_ID })) {
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#findById(int)}
	 */
	@Override
	public Tag findById(Long id) throws DAOException {
		Tag tag = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tag;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#update(Tag)}
	 */
	@Override
	public void update(Tag entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG)) {
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.setLong(2, entity.getTagId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#deleteById(int)}
	 */
	@Override
	public void deleteById(Long[] ids) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TAG)) {
			for (Long id : ids) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#findAllNewsTag(int)}
	 */
	@Override
	public List<Tag> findAllNewsTag(Long newsId) throws DAOException {
		List<Tag> tags = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID)) {
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Tag tag = parseResultSet(resultSet);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tags;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.TagDAO#findAllTag()}
	 */
	@Override
	public List<Tag> findAllTag() throws DAOException {
		List<Tag> tags = new ArrayList<>();
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_TAGS);
			while (resultSet.next()) {
				Tag tag = parseResultSet(resultSet);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return tags;
	}

	@Override
	public void deleteTagFromNewsTags(Long[] tagIds) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TAGS_FROM_NEWS_TAGS)) {
			for (Long id : tagIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * Parse the result set object
	 * 
	 * @param resultSet
	 *            the result set object
	 * @return the tag object
	 * @throws SQLException
	 */
	private Tag parseResultSet(ResultSet resultSet) throws SQLException {
		Tag tag = new Tag();
		tag.setTagId(resultSet.getLong(DBColumnName.TAG_ID));
		tag.setTagName(resultSet.getString(DBColumnName.TAG_NAME));
		return tag;
	}

	/**
	 * Set the DataSource
	 * 
	 * @param dataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Get a dataSource
	 * 
	 * @return
	 */
	public BasicDataSource getDataSource() {
		return dataSource;
	}
}
