package core.collections;

public class CustomArrList<T> {
	private T[] arr;
	private int size;
	private int capacity;

	@SuppressWarnings("unchecked")
	public CustomArrList() {
		arr = (T[]) new Object[10];
		size = 0;
		capacity = 10;
	}

	@SuppressWarnings("unchecked")
	public CustomArrList(int capacity) {
		if (capacity > 0) {
			arr = (T[]) new Object[capacity];
			size = 0;
			this.capacity = capacity;
		} else {
			throw new IllegalArgumentException("Invalid value(negative) of capacity.");
		}
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public void add(T elem) {
		if (size != capacity - 1) {
			arr[size] = elem;
			size++;
		} else {
			int oldCapacity = capacity;
			capacity = (oldCapacity * 3) / 2 + 1;
			T[] newArr = (T[]) new Object[capacity];
			System.arraycopy(arr, 0, newArr, 0, size);
			arr = newArr;
			arr[size] = elem;
			size++;
		}
	}

	@SuppressWarnings("unchecked")
	public void add(T elem, int position) {
		if (position < 0 || position > size) {
			throw new IllegalArgumentException("Invalid value.");
		}
		if (size != capacity - 1) {
			T[] newArr = (T[]) new Object[capacity];
			System.arraycopy(arr, 0, newArr, 0, position);
			newArr[position] = elem;
			System.arraycopy(arr, position, newArr, position + 1, size - position);
			arr = newArr;
			size++;
		} else {
			int oldCapacity = capacity;
			capacity = (oldCapacity * 3) / 2 + 1;
			T[] newArr = (T[]) new Object[capacity];
			System.arraycopy(arr, 0, newArr, 0, position);
			newArr[position] = elem;
			System.arraycopy(arr, position, newArr, position + 1, size - position);
			arr = newArr;
			size++;
		}
	}

	public T get(int index) {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("Invalid value.");
		}
		T elem = arr[index];
		return elem;
	}
	@SuppressWarnings("unchecked")
	public void delete(int index){
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("Invalid value.");
		}
		T[] newArr = (T[])new Object[capacity];
		System.arraycopy(arr, 0, newArr, 0, index);
		System.arraycopy(arr, index+1, newArr, index, size-index);
		arr=newArr;
		size--;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("CustomArrList [arr=[ ");
		for (T elem : arr) {
			if (elem != null) {
				str.append(elem.toString());
				str.append(", ");
			}
		}
		str.delete(str.length() - 2, str.length() - 1);
		str.append("]");
		return new String(str);
	}

}
