<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/news" var="newsUrl" htmlEscape="true" />
<spring:url value="/addNews" var="addNewsUrl" htmlEscape="true" />
<spring:url value="/addAuthor" var="authorUrl" htmlEscape="true" />
<spring:url value="/addTag" var="tagUrl" htmlEscape="true" />
<div>
	<ul id="nav">
		<li><a href="${newsUrl}" id="news-list-nav" class="nav"><spring:message
					code="locale.navigation.news-list" /></a></li>
		<li><a href="${addNewsUrl}" id="add-news-nav" class="nav"><spring:message
					code="locale.navigation.add-news" /></a></li>
		<li><a href="${authorUrl}" id="add-author-nav" class="nav"><spring:message
					code="locale.navigation.add-authors" /></a></li>
		<li><a href="${tagUrl}" id="add-tags-nav" class="nav"><spring:message
					code="locale.navigation.add-tags" /></a></li>
	</ul>
</div>
<span id="currentMenu" style="display: none;">${currentMenu}</span>

<script type="text/javascript">
	SetColorForCurrentMenu();
</script>
