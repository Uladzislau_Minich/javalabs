package com.epam.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newscommon.constant.DBColumnName;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.dao.AuthorDAO;
import com.epam.newscommon.entity.Author;

/**
 * Class AuthorDAOImpl. Implementation of AuthorDAO
 * 
 * @author Uladzislau_Minich
 *
 */
public class AuthorDAOImpl implements AuthorDAO {

	private BasicDataSource dataSource;

	private static final String SQL_FIND_ALL_AUTHORS = "SELECT * FROM AUTHORS";
	private static final String SQL_FIND_AUTHOR_BY_ID = "Select * from authors where authors.author_id = ?";
	private static final String SQL_FIND_AUTHOR_BY_NEWS_ID = "Select * from authors JOIN news_author ON (authors.author_id=news_author.author_id) where news_id=?";
	private static final String SQL_CREATE_AUTHOR = "Insert into authors(author_id, author_name,expired) values(AUTHORS_SEQ.nextval,?,?) ";
	private static final String SQL_UPDATE_AUTHOR = "Update authors SET author_name=?, expired=? where author_id=?";
	private static final String SQL_UPDATE_AUTHOR_EXPIRED = "Update authors SET expired=? where author_id=?";
	private static final String SQL_DELETE_AUTHOR = "DELETE From authors where author_id=?";

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#create(Author)}
	 */
	@Override
	public Long create(Author entity) throws DAOException {
		Long id = (long) 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_AUTHOR,
				new String[] { DBColumnName.AUTHOR_ID })) {
			preparedStatement.setString(1, entity.getAuthorName());
			preparedStatement.setDate(2, null);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#findById(int)}
	 */
	@Override
	public Author findById(Long id) throws DAOException {
		Author author = null;
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AUTHOR_BY_ID)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}

		return author;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#update(Author)}
	 */
	@Override
	public void update(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
			preparedStatement.setString(1, entity.getAuthorName());
			preparedStatement.setTimestamp(2,
					entity.getExpired() != null ? new Timestamp(entity.getExpired().getTime()) : null);
			preparedStatement.setLong(3, entity.getAuthorId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#deleteById(int)}
	 */
	@Override
	public void deleteById(Long[] ids) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
			for (Long id : ids) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#expiredAuthor(int)}
	 */
	@Override
	public void expiredAuthor(Long idAuthor) throws DAOException {
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR_EXPIRED)) {
			preparedStatement.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));
			preparedStatement.setLong(2, idAuthor);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#findAuthorByNews(int)}
	 */
	@Override
	public Author findAuthorByNews(Long idNews) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_AUTHOR_BY_NEWS_ID)) {
			preparedStatement.setLong(1, idNews);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.AuthorDAO#findAllAuthors()}
	 */
	@Override
	public List<Author> findAllAuthors() throws DAOException {
		List<Author> authors = new ArrayList<>();
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_AUTHORS);
			while (resultSet.next()) {
				Author author = parseResultSet(resultSet);
				authors.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return authors;
	}

	/**
	 * Parse result set
	 * 
	 * @param resultSet
	 *            the result object
	 * @return the author object
	 * @throws SQLException
	 */
	private Author parseResultSet(ResultSet resultSet) throws SQLException {
		Author author = new Author();
		author.setAuthorId(resultSet.getLong(DBColumnName.AUTHOR_ID));
		author.setAuthorName(resultSet.getString(DBColumnName.AUTHOR_NAME));
		author.setExpired(resultSet.getTimestamp(DBColumnName.AUTHOR_EXPIRED));
		return author;
	}

	/**
	 * Set a field dataSource
	 * 
	 * @param dataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Get a dataSource
	 * 
	 * @return
	 */
	public BasicDataSource getDataSource() {
		return dataSource;
	}
}
