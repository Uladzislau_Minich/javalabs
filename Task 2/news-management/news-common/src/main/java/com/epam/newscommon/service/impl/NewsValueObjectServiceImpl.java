package com.epam.newscommon.service.impl;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.entity.News;
import com.epam.newscommon.entity.NewsValueObject;
import com.epam.newscommon.entity.Tag;
import com.epam.newscommon.service.AuthorService;
import com.epam.newscommon.service.CommentService;
import com.epam.newscommon.service.NewsService;
import com.epam.newscommon.service.NewsValueObjectService;
import com.epam.newscommon.service.TagService;
import com.epam.newscommon.utils.SearchCriteriaObject;

/**
 * CLass NewsValueObjectServiceImpl. Implementation of interface
 * NewsValueObjectService
 * 
 * @author Uladzislau_Minich
 *
 */
public class NewsValueObjectServiceImpl implements NewsValueObjectService {
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#saveNews(News, Author, List)}
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
	public NewsValueObject saveNews(News news, Author author, List<Tag> tags) throws ServiceException {
		NewsValueObject newsObj = new NewsValueObject();
		Long newsId = newsService.save(news);
		newsService.addNewsAuthors(newsId, author.getAuthorId());
		if (!(tags == null || tags.isEmpty())) {
			for (Tag tag : tags) {
				newsService.addNewsTag(tag.getTagId(), newsId);
			}
		}
		newsObj.setAuthor(author);
		newsObj.setNews(news);
		newsObj.setTags(tags);
		return newsObj;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#deleteNews(int)}
	 */
	@Override
	@Transactional(propagation = Propagation.NESTED, rollbackFor = ServiceException.class)
	public void deleteNews(Long[] idNews) throws ServiceException {
		newsService.deleteNewsTag(idNews);
		newsService.deleteNewsAuthor(idNews);
		commentService.deleteCommentsByNewsId(idNews);
		newsService.deleteById(idNews);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#findNews(int)}
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class, readOnly=true)
	public NewsValueObject findNews(Long newsId) throws ServiceException {
		NewsValueObject newsObj = new NewsValueObject();
		newsObj.setNews(newsService.findById(newsId));
		newsObj.setAuthor(authorService.findAuthorByNews(newsId));
		newsObj.setTags(tagService.findAllNewsTag(newsId));
		newsObj.setComments(commentService.findAllNewsComment(newsId));
		return newsObj;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#updateNews(NewsValueObject)}
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
	public NewsValueObject updateNews(NewsValueObject obj) throws ServiceException {
		newsService.update(obj.getNews());
		newsService.deleteNewsAuthor(new Long[]{obj.getNews().getNewsId()});
		newsService.addNewsAuthors(obj.getNews().getNewsId(), obj.getAuthor().getAuthorId());
		newsService.deleteNewsTag(new Long[]{obj.getNews().getNewsId()});
		for (Tag tag : obj.getTags()) {
			newsService.addNewsTag(tag.getTagId(), obj.getNews().getNewsId());
		}
		return obj;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class, readOnly=true)
	public List<NewsValueObject> findNewsPerPage(SearchCriteriaObject sc, int startIndex, int endIndex)
			throws ServiceException {
		List<NewsValueObject> newsVOAll = new ArrayList<>();
		List<News> newsAll = newsService.findNewsPerPage(sc, startIndex, endIndex);
		for (News news : newsAll) {
			NewsValueObject newsVO = new NewsValueObject();
			newsVO.setNews(news);
			newsVO.setAuthor(authorService.findAuthorByNews(news.getNewsId()));
			newsVO.setTags(tagService.findAllNewsTag(news.getNewsId()));
			newsVO.setComments(commentService.findAllNewsComment(news.getNewsId()));
			newsVOAll.add(newsVO);
		}
		return newsVOAll;
	}

	/**
	 * @return
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#addComment(Comment)}
	 */
	@Override
	public Long addComment(Comment comment) throws ServiceException {
		return commentService.save(comment);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#deleteComment(int)}
	 */
	@Override
	public void deleteComment(Long[] commentIds) throws ServiceException {
		commentService.deleteById(commentIds);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#addTag(Tag, int)}
	 */
	@Override
	public void addTag(Tag tag) throws ServiceException {
		tagService.save(tag);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#expiredAuthor(Author)}
	 */
	@Override
	public void expiredAuthor(Author author) throws ServiceException {
		authorService.expiredAuthor(author.getAuthorId());
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#saveAuthor(Author)}
	 */
	@Override
	public void addAuthor(Author author) throws ServiceException {
		authorService.save(author);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#updateAuthor(Author)}
	 */
	@Override
	public void updateAuthor(Author author) throws ServiceException {
		authorService.update(author);
	}
	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#findAuthorById(Long)}
	 */
	@Override
	public Author findAuthorById(Long authorId) throws ServiceException {
		Author author = authorService.findById(authorId);
		return author;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#findAllAuthorNews(int)}
	 */
	@Override
	public List<NewsValueObject> findAllAuthorNews(Long authorId) throws ServiceException {
		List<NewsValueObject> newsVOAll = new ArrayList<>();
		List<News> newsAll = newsService.findAllAuthorNews(authorId);
		for (News news : newsAll) {
			NewsValueObject newsVO = new NewsValueObject();
			newsVO.setNews(news);
			newsVO.setAuthor(authorService.findAuthorByNews(news.getNewsId()));
			newsVO.setTags(tagService.findAllNewsTag(news.getNewsId()));
			newsVO.setComments(commentService.findAllNewsComment(news.getNewsId()));
			newsVOAll.add(newsVO);
		}
		return newsVOAll;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#findAllTags()}
	 */
	@Override
	public List<Tag> findAllTags() throws ServiceException {
		List<Tag> list = tagService.findAllTag();
		return list;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#deleteTag(int, int)}
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
	public void deleteTag(Long[] tagIds) throws ServiceException {
		tagService.deleteTagFromNewsTags(tagIds);
		tagService.deleteById(tagIds);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsValueObjectService#countNews()}
	 */
	@Override
	public int countNews(SearchCriteriaObject sc) throws ServiceException {
		int amount = newsService.countNews(sc);
		return amount;

	}


	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findAllAuthors()}
	 */
	@Override
	public List<Author> findAllAuthors() throws ServiceException {
		List<Author> authors = authorService.findAllAuthors();
		return authors;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findPreviousNews(NewsValueObject)}
	 */
	@Override
	public Long findPreviousNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException {
		Long previousNewsId = 0L;
		previousNewsId = newsService.findPreviousNews(currentNewsId,sc);
		return previousNewsId;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findNextNews(NewsValueObject)}
	 */
	@Override
	public Long findNextNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException {
		Long nextNewsId = 0L;
		nextNewsId = newsService.findNextNews(currentNewsId,sc);
		return nextNewsId;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#updateTag(Tag)}
	 */
	@Override
	public void updateTag(Tag tag) throws ServiceException {
		tagService.update(tag);
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findTagById(Long)}
	 */
	@Override
	public Tag findTagById(Long tagId) throws ServiceException {
		return tagService.findById(tagId);
	}
	/**
	 * Set a newsService field
	 * 
	 * @param newsService
	 *            the newsService to set
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * Set a commentService field
	 * 
	 * @param commentService
	 *            the commentService to set
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * Set a tagService field
	 * 
	 * @param tagService
	 *            the tagService to set
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Set a authorService field
	 * 
	 * @param authorService
	 *            the authorService to set
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}


}
