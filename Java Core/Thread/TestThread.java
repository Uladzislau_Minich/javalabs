package com.thread;

public class TestThread {

	public static void main(String[] args) {
		A a = new A();
		new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						a.write();
					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}

				}
			}
		}.start();
		
		new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						a.read();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();

	}

	
}
class A{
	public synchronized void read() throws InterruptedException {
		notify();
		System.out.println("read 1");
		wait();
		
	}

	public synchronized void write() throws InterruptedException {
		notify();
		System.out.println("write 1");
		wait();
	}
}
