package com.epam.newscommon.utils;

import java.io.Serializable;
import java.util.List;

/**
 * Class SearchCriteriaObject. Class uses for searching news
 * 
 * @author Uladzislau_Minich
 *
 */
public class SearchCriteriaObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long authorId;
	private List<Long> tagIds;

	public SearchCriteriaObject() {
		super();
	}

	public SearchCriteriaObject(Long authorId, List<Long> tagsId) {
		super();
		this.authorId = authorId;
		this.tagIds = tagsId;
	}

	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<Long> getTagIds() {
		return tagIds;
	}
	/**
	 * Sets the tags. 
	 */
	public void getTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((tagIds == null) ? 0 : tagIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteriaObject other = (SearchCriteriaObject) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (tagIds == null) {
			if (other.tagIds != null)
				return false;
		} else if (!tagIds.equals(other.tagIds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SearchCriteriaObject [authorIds=" + authorId + ", tagIds=" + tagIds + "]";
	}
	
}
