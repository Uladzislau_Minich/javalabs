package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newscommon.dao.impl.AuthorDAOImpl;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.AuthorServiceImpl;

/**
 * Class AuthorServiceTest. Contains methods for testing working a class
 * AuthorService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock
	private AuthorDAOImpl authorDAOMock;
	@InjectMocks
	private AuthorServiceImpl authorServiceMock;

	private Author author;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		author = mock(Author.class);
		authorDAOMock = mock(AuthorDAOImpl.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(authorServiceMock.save(author)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				author = new Author();
				author.setAuthorId(123L);
				author.setAuthorName("Author name");
				return author.getAuthorId();
			}
		});
		
		Long idAuthor = new Long(authorServiceMock.save(author));
		Long expectedId = new Long(123);
		Assert.assertEquals(expectedId, idAuthor);
	}

	@Test
	public void checkFindById() throws ServiceException {
		Long id = 123L;
		when(authorServiceMock.findById(id)).then(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				author = new Author();
				author.setAuthorId(123L);
				author.setAuthorName("Author name");
				return author;
			}
		});
		author = authorServiceMock.findById(id);
		assertSame("Author name", author.getAuthorName());
	}

	@Test
	public void checkUpdate() throws ServiceException, DAOException {
		Author author = new Author();
		authorDAOMock.update(author);
		verify(authorDAOMock).update(author);
	}

	@Test
	public void checkDelete() throws ServiceException, DAOException {
		authorDAOMock.deleteById(new Long[]{12L});
		verify(authorDAOMock).deleteById(new Long[]{12L});
	}

}
