/** store value of editing for cancel */
var valueO;
/** store id of current editing field */
var currentFieldId;

/**
 * do input fields (with name of author or tag) on authors page and tags page disabled 
 */
function doDisabled(){
	var inputFieldArr= $('.input-text');
	for(var i=0;i<inputFieldArr.length;i++){
		inputFieldArr[i].disabled=true;
	}
}

/**
 * show buttons (update, expired, delete, cancel) and do selected author/tag disabled='false'
 */
function showButton(id){
	if(currentFieldId){
		cancel(currentFieldId);
	}
	currentFieldId=id;
	valueO = $("#input-text"+id).val();
	document.getElementById(id).style.display='none';
	document.getElementById("cancel"+id).style.display='inline';
	if(document.getElementById("expired"+id)!=null){
		document.getElementById("expired"+id).style.display='inline';
	}
	document.getElementById("update"+id).style.display='inline';
	document.getElementById("input-text"+id).disabled=false;
	if(document.getElementById("delete"+id)!=null){
		document.getElementById("delete"+id).style.display='inline'
	};
}

/**
 * hide buttons (update, expired, delete, cancel) an do selected author/tag disabled
 */
function cancel(id) {
	$("#input-text"+id).removeClass('error');
	if(document.getElementById("tagName.errors")!=null){
		document.getElementById("tagName.errors").style.display='none';
	}
	if(document.getElementById("authorName.errors")!=null){
		document.getElementById("authorName.errors").style.display='none';
	}
	$("#input-text"+id).val(valueO);
	document.getElementById(id).style.display='inline';
	document.getElementById("cancel"+id).style.display='none';
	if(document.getElementById("expired"+id)!=null){
		document.getElementById("expired"+id).style.display='none';
	}
	document.getElementById("update"+id).style.display='none';
	document.getElementById("input-text"+id).disabled=true;
	if(document.getElementById("delete"+id)!=null){
		document.getElementById("delete"+id).style.display='none'
	};
}

/**
 * highlights current menu page
 */
function SetColorForCurrentMenu() {
	var currentMenuNav = $('#currentMenu').text();
	if ($('#news-list-nav').attr('id').indexOf(currentMenuNav) != -1) {
		$('#news-list-nav').css('color', 'green');
		$('#news-list-nav').css('fontSize', '19px');
		$('#news-list-nav').css('fontWeight', 'bold');
	} else if ($('#add-news-nav').attr('id').indexOf(currentMenuNav) != -1) {
		$('#add-news-nav').css('color', 'green');
		$('#add-news-nav').css('fontSize', '19px');
		$('#add-news-nav').css('fontWeight', 'bold')
	} else if ($('#add-author-nav').attr('id').indexOf(currentMenuNav) != -1) {
		$('#add-author-nav').css('color', 'green');
		$('#add-author-nav').css('fontSize', '19px');
		$('#add-author-nav').css('fontWeight', 'bold')
	} else if ($('#add-tags-nav').attr('id').indexOf(currentMenuNav) != -1) {
		$('#add-tags-nav').css('color', 'green');
		$('#add-tags-nav').css('fontSize', '19px');
		$('#add-tags-nav').css('fontWeight', 'bold')
	}
}
