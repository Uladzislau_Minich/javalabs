package com.epam.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newscommon.constant.DBColumnName;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.utils.SearchCriteriaObject;
import com.epam.newscommon.dao.NewsDAO;
import com.epam.newscommon.entity.News;

/**
 * Class NewsDAOImpl. Implementation of NewsDAO
 * 
 * @author Uladzislau_Minich
 *
 */
public class NewsDAOImpl implements NewsDAO {

	private BasicDataSource dataSource;
	private static final String SQL_CREATE_NEWS = "Insert into news (news_id,title,short_text,full_text,creation_date,modification_date) values(NEWS_SEQ.nextval,?,?,?,?,?)";
	private static final String SQL_CREATE_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHOR(news_id,author_id) VALUES(?,?)";
	private static final String SQL_CREATE_NEWS_TAG = "Insert into news_tag values(?,?)";
	private static final String SQL_FIND_NEWS_PER_PAGE = "SELECT NEWS.NEWS_ID, NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT (COMMENTS.COMMENT_ID) FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID=COMMENTS.NEWS_ID)";
	private static final String SQL_FIND_NEWS_PER_PAGE_END = " GROUP BY news.NEWS_ID,news.TITLE,news.SHORT_TEXT,news.FULL_TEXT,news.CREATION_DATE,news.MODIFICATION_DATE ORDER BY COUNT (COMMENTS.COMMENT_ID) DESC ,news.MODIFICATION_DATE DESC";
	private static final String SQL_FIND_BY_ID = "Select * from news where news.news_id=?";
	private static final String SQL_FIND_ALL_AUTHORS_NEWS = "Select * from news JOIN news_author ON(news_author.news_id=news.news_id) where news_author.author_id=?";
	private static final String SQL_UPDATE = "Update news SET title=?,short_text=?,full_text=?, modification_date=? where news_id=?";
	private static final String SQL_DELETE = "Delete from news where news_id=?";
	private static final String SQL_DELETE_NEWS_AUTHORS = "DELETE FROM news_author where news_id=?";
	private static final String SQl_DELETE_NEWS_TAG = "DELETE FROM news_tag where news_id=?";
	private static final String SQL_COUNT_NEWS = "Select  COUNT(news_search.news_id) from (Select news.news_id from news ";
	private static final String SQL_COUNT_NEWS_END = " Group BY news.news_id )news_search";
	private static final String SQP_PAGINATION_PART_1 = "Select *   from ( select news_by_one_page.*, rownum rnum from ( ";
	private static final String SQP_PAGINATION_PART_2 = " ) news_by_one_page where rownum <= ?) where rnum >= ?";
	private static final String SQL_FIND_NEXT_NEWS="SELECT news_id, next_news_id FROM (SELECT wrap_news.*, LEAD(wrap_news.news_id) over( ORDER BY wrap_news.countNews DESC, wrap_news.modification_date DESC) AS next_news_id FROM (SELECT news.news_id, news.modification_date, COUNT(comments.comment_id) AS countNews FROM news LEFT JOIN COMMENTS ON (NEWS.NEWS_ID=COMMENTS.NEWS_ID) ";
	private static final String SQL_FIND_NEXT_NEWS_END=" GROUP BY news.news_id, news.modification_date, 'COUNT(comments.comment_id)' ORDER BY COUNT(comments.comment_id), news.modification_date) wrap_news) WHERE news_id=?";
	private static final String SQL_FIND_PREVIOUS_NEWS="SELECT news_id, prev_news_id FROM (SELECT wrap_news.*, LAG(wrap_news.news_id) over(ORDER BY wrap_news.countNews DESC , wrap_news.modification_date DESC)AS prev_news_id FROM (SELECT news.news_id, news.modification_date, COUNT(comments.comment_id) AS countNews FROM news LEFT JOIN COMMENTS ON (NEWS.NEWS_ID=COMMENTS.NEWS_ID) ";
	private static final String SQL_FIND_PREVIOUS_NEWS_END=" GROUP BY news.news_id, news.modification_date, 'COUNT(comments.comment_id)' ORDER BY COUNT(comments.comment_id), news.modification_date) wrap_news) WHERE news_id=?";
	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#create(News)}
	 */
	@Override
	public Long create(News entity) throws DAOException {
		Long id = (long) 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS,
				new String[] { DBColumnName.NEWS_ID })) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4, entity.getCreationTime());
			preparedStatement.setDate(5, new Date(entity.getModificationDate().getTime()));
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#findById(int)}
	 */
	@Override
	public News findById(Long id) throws DAOException {
		News news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#update(News)}
	 */
	@Override
	public void update(News entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setDate(4, new Date(entity.getModificationDate().getTime()));
			preparedStatement.setLong(5, entity.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#deleteById(int)}
	 */
	@Override
	public void deleteById(Long[] ids) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
			for (Long id : ids) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#findNewsPerPage()}
	 */
	@Override
	public List<News> findNewsPerPage(SearchCriteriaObject sc, int startIndex, int endIndex) throws DAOException {
		List<News> allNews = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		StringBuilder query = new StringBuilder(SQL_FIND_NEWS_PER_PAGE);
		if (sc != null) {
			query = buildSQLQuerly(sc, query);
		}
		String finalQuery = new String(query);
		try (PreparedStatement preparedStatement = connection.prepareStatement(
				SQP_PAGINATION_PART_1 + finalQuery + SQL_FIND_NEWS_PER_PAGE_END + SQP_PAGINATION_PART_2)) {

			preparedStatement.setInt(1, endIndex);
			preparedStatement.setInt(2, startIndex);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				News news = parseResultSet(resultSet);
				allNews.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return allNews;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#countNews(SearchCriteriaObject)}
	 */
	@Override
	public int countNews(SearchCriteriaObject sc) throws DAOException {
		int amount = 0;
		StringBuilder query = new StringBuilder(SQL_COUNT_NEWS);
		if (sc != null) {
			query = buildSQLQuerly(sc, query);
		}
		String finalQuery = new String(query);
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(finalQuery + SQL_COUNT_NEWS_END);
			if (resultSet.next()) {
				amount = resultSet.getInt("COUNT(news_search.news_id)");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return amount;

	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#saveNewsAuthors(int, int)}
	 */
	@Override
	public void addNewsAuthors(Long newsId, Long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_AUTHORS)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#deleteNewsAuthor(int)}
	 */
	@Override
	public void deleteNewsAuthor(Long[] newsIds) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHORS)) {
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#addNewsTag(int, int)}
	 */
	@Override
	public void addNewsTag(Long tagId, Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_TAG)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#deleteNewsTag(int)}
	 */
	@Override
	public void deleteNewsTag(Long[] newsIds) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQl_DELETE_NEWS_TAG)) {
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#findAllAuthorsNews(int)}
	 */
	@Override
	public List<News> findAllAuthorsNews(Long authorId) throws DAOException {
		List<News> list = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_AUTHORS_NEWS)) {
			preparedStatement.setLong(1, authorId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				News news = parseResultSet(resultSet);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return list;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#findNextNews(News)}
	 */
	@Override
	public Long findPreviousNews(Long currentNewsId, SearchCriteriaObject sc) throws DAOException {
		Long previousNewsId = 0L;
		StringBuilder query = new StringBuilder(SQL_FIND_PREVIOUS_NEWS);
		if (sc != null) {
			query = buildSQLQuerly(sc, query);
		}
		String finalQuery = new String(query);
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				PreparedStatement preparedStatement = connection.prepareStatement(finalQuery+SQL_FIND_PREVIOUS_NEWS_END)) {
			preparedStatement.setLong(1,currentNewsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				if(resultSet.getLong("PREV_NEWS_ID")!=0){
					previousNewsId=resultSet.getLong("PREV_NEWS_ID");
				}
				 else {
					 previousNewsId = -1L;
				 }
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return previousNewsId;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.NewsDAO#findNextNews(News)}
	 */
	@Override
	public Long findNextNews(Long currentNewsId, SearchCriteriaObject sc) throws DAOException {
		Long nextNewsId = 0L;
		StringBuilder query = new StringBuilder(SQL_FIND_NEXT_NEWS);
		if (sc != null) {
			query = buildSQLQuerly(sc, query);
		}
		String finalQuery = new String(query);
		try (Connection connection = DataSourceUtils.getConnection(dataSource);
				PreparedStatement preparedStatement = connection.prepareStatement(finalQuery+SQL_FIND_NEXT_NEWS_END)) {
			preparedStatement.setLong(1, currentNewsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				if(resultSet.getLong("NEXT_NEWS_ID")!=0){
					nextNewsId=resultSet.getLong("NEXT_NEWS_ID");
				}
				 else {
						nextNewsId = -1L;
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return nextNewsId;
	}

	/**
	 * Build SQL query accordingly to search criteria object
	 * 
	 * @param sc
	 * @return StringBuilder query
	 */
	private StringBuilder buildSQLQuerly(SearchCriteriaObject sc, StringBuilder query) {
		StringBuilder joinTablePart = query;
		StringBuilder filterPart = new StringBuilder("");
		if (sc.getAuthorId() != null) {
			joinTablePart.append("JOIN news_author ON (news.news_id=news_author.news_id) ");
			filterPart.append("WHERE news_author.author_id=" + sc.getAuthorId() + " ");
		}
		if (sc.getTagIds() != null) {
			joinTablePart.append("JOIN news_tag ON(news.news_id=news_tag.news_id) ");
			if (sc.getAuthorId() != null) {
				filterPart.append("AND news_tag.tag_id IN(");
			} else {
				filterPart.append("WHERE news_tag.tag_id IN(");
			}
			for (Long id : sc.getTagIds()) {
				filterPart.append(id);
				filterPart.append(",");
			}
			filterPart.deleteCharAt(filterPart.length() - 1);
			filterPart.append(") ");
		}
		joinTablePart.append(filterPart);
		return joinTablePart;
	}

	/**
	 * Parse result set object
	 * 
	 * @param resultSet
	 *            the result set object
	 * @return the news object
	 * @throws SQLException
	 */
	private News parseResultSet(ResultSet resultSet) throws SQLException {
		News news = new News();
		news.setCreationTime(resultSet.getTimestamp(DBColumnName.NEWS_CREATION_DATE));
		news.setFullText(resultSet.getString(DBColumnName.NEWS_FULL_TEXT));
		news.setModificationDate(resultSet.getDate(DBColumnName.NEWS_MODIFICATION_DATE));
		news.setNewsId(resultSet.getLong(DBColumnName.NEWS_ID));
		news.setShortText(resultSet.getString(DBColumnName.NEWS_SHORT_TEXT));
		news.setTitle(resultSet.getString(DBColumnName.NEWS_TITLE));
		return news;
	}

	/**
	 * Set the DataSource
	 * 
	 * @param dataSource
	 */
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Get a dataSource
	 * 
	 * @return
	 */
	public BasicDataSource getDataSource() {
		return dataSource;
	}

}
