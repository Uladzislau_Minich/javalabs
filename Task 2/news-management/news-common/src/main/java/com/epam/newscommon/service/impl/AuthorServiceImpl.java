package com.epam.newscommon.service.impl;

import java.util.List;

import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.dao.AuthorDAO;
import com.epam.newscommon.dao.impl.AuthorDAOImpl;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.service.AuthorService;

/**
 * Class AuthorServiceImpl. Implementation of the AuthorService
 * 
 * @author Uladzislau_Minich
 *
 */
public class AuthorServiceImpl implements AuthorService {
	private AuthorDAO authorDAO;

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#save()}
	 */
	@Override
	public Long save(Author entity) throws ServiceException {
		try {
			entity.setAuthorId(authorDAO.create(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity.getAuthorId();
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#findById()}
	 */
	@Override
	public Author findById(Long id) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.findById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#update()}
	 */
	@Override
	public void update(Author entity) throws ServiceException {
		try {
			authorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#deleteById()}
	 */
	@Override
	public void deleteById(Long[] ids) throws ServiceException {
		try {
			authorDAO.deleteById(ids);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#expiredAuthor()}
	 */
	@Override
	public void expiredAuthor(Long idAuthor) throws ServiceException {
		try {
			authorDAO.expiredAuthor(idAuthor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#findAuthorByNews()}
	 */
	@Override
	public Author findAuthorByNews(Long idNews) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.findAuthorByNews(idNews);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	/**
	 * @see {@link com.epam.newscommon.service.AuthorService#findAllAuthors()}
	 */
	@Override
	public List<Author> findAllAuthors() throws ServiceException {
		List<Author> authors = null;
		try {
			authors = authorDAO.findAllAuthors();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return authors;

	}
	/**
	 * Set a field authorDAO
	 * 
	 * @param authorDAO
	 */
	public void setAuthorDAO(AuthorDAOImpl authorDAO) {
		this.authorDAO = authorDAO;
	}
}
