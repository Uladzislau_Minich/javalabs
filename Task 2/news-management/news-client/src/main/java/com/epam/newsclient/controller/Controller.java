package com.epam.newsclient.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.epam.newsclient.command.CommandFactory;
import com.epam.newsclient.command.ICommand;
import com.epam.newsclient.utils.ConfigurationManager;
import com.epam.newscommon.exception.ServiceException;

@SuppressWarnings("serial")
@WebServlet("/controller")
public class Controller extends HttpServlet {
	
	private CommandFactory client;
	private final static String NAME_BEAN_COMMAND_FACTORY="CommandFactory";
	private final static String PATH_PAGE_ERROR="path.page.error";
	private final static String PATH_PAGE_NOT_FOUND="path.page.not.found";
	private final static String PARAM_REQUEST_MESSAGE="message";
	private final static String PARAM_REQUEST_MESSAGE_TEXT="message.page.not.found";
	
	@SuppressWarnings("resource")
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		Locale.setDefault(Locale.ENGLISH);
		XmlWebApplicationContext ap = new XmlWebApplicationContext();
		ap.setServletContext(config.getServletContext());
		ap.refresh();
		client = (CommandFactory) ap.getBean(NAME_BEAN_COMMAND_FACTORY);	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		ICommand command = client.defineCommand(request);
		try {
			if (command != null){
				page = command.execute(request);
			}
			if (page != null) {

				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
				dispatcher.forward(request, response);
			} else {
				page = ConfigurationManager.getProperty(PATH_PAGE_NOT_FOUND);
				request.getSession().setAttribute(PARAM_REQUEST_MESSAGE, PARAM_REQUEST_MESSAGE_TEXT);
				response.sendRedirect(request.getContextPath() + page);

			}
		} catch (ServiceException e) {
			page = ConfigurationManager.getProperty(PATH_PAGE_ERROR);
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}