package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.utils.SearchCriteriaObject;
import com.epam.newscommon.entity.News;

/**
 * Interface AuthorService. Classes which implements this interface contain
 * action with NewsDAO in DAO layer
 * 
 * @author Uladzislau_Minich
 *
 */
public interface NewsService extends BaseService<News> {
	/**
	 * Find all news
	 * 
	 * @return list of news
	 * @throws ServiceException
	 */

	List<News> findNewsPerPage(SearchCriteriaObject sc,int startIndex, int endIndex) throws ServiceException;
	/**
	 * Find all new of some author
	 * 
	 * @param authorId
	 * @return list of news
	 * @throws ServiceException
	 */
	List<News> findAllAuthorNews(Long authorId) throws ServiceException;

	/**
	 * Add information to table news_author
	 * 
	 * @param newsId
	 * @param authorId
	 * @return
	 * @throws ServiceException
	 */
	void addNewsAuthors(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Delete information from table news_author
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	void deleteNewsAuthor(Long[] newsId) throws ServiceException;

	/**
	 * Add information to table news_tag
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	void addNewsTag(Long tagId, Long newsId) throws ServiceException;

	/**
	 * Delete information from table news_tag
	 * 
	 * @param idNews
	 * @return
	 * @throws ServiceException
	 */
	void deleteNewsTag(Long[] idNews) throws ServiceException;
	/**
	 * Count news
	 * @return amount of news
	 * @throws ServiceException
	 */
	int countNews(SearchCriteriaObject sc) throws ServiceException;

	/**
	 * Find news on the list, which is before the current
	 * 
	 * @param currentNewsId
	 * @return news's id
	 * @throws ServiceException
	 */
	Long findPreviousNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException;
	/**
	 * Find news on the list, which is following the current
	 * 
	 * @param currentNewsId
	 * @return news's id
	 * @throws ServiceException
	 */
	Long findNextNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException;
}
