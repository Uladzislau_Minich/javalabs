package com.epam.news.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.news.dao.implementation.TagDAOImpl;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
/**
 * Class TagDAOTest.  Contains methods for testing working a class TagDAO.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-context-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class TagDAOTest  {

	@Autowired
	private TagDAO tagDAO;

	/**
	 * Load data to database. 
	 * 
	 * @throws FileNotFoundException
	 * @throws DatabaseUnitException
	 * @throws SQLException
	 */
	@Before
	public void doSetup() throws FileNotFoundException, DatabaseUnitException, SQLException {
		Locale.setDefault(Locale.ENGLISH);
		Connection con = TagDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-tag-content.xml")) };

		DatabaseOperation.CLEAN_INSERT.execute(
				new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new CompositeDataSet(datasets));
		con.close();
	}

	/**
	 * Clean database after testing
	 * 
	 * @throws DataSetException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 */
	@After
	public void cleanDB() throws DataSetException, FileNotFoundException, SQLException, DatabaseUnitException {
		Connection con = TagDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		DatabaseOperation.DELETE_ALL.execute(new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete-content.xml")));
		con.close();
	}

	@Test
	public void chechGetAll() throws DAOException {
		List<Tag> list = tagDAO.findAllTag();
		Assert.assertEquals(3, list.size());
	}

	@Test
	public void chechFindAllNewsTags() throws DAOException {
		List<Tag> list = tagDAO.findAllNewsTag(1L);
		Assert.assertEquals(list.size(), 2);
	}

	@Test
	public void checkCreateTag() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("New tag");
		Long unexpectedTagId = -1L;
		Long actualTagId = tagDAO.create(tag);
		Assert.assertNotEquals(unexpectedTagId, actualTagId);
	}

	@Test
	public void checkFindByid() throws DAOException {
		Tag tag = tagDAO.findById(1L);
		String expectedName = "QWERTY";
		Assert.assertEquals(expectedName, tag.getTagName());
	}

	@Test
	public void checkUpdate() throws DAOException {
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("Update tag");
		assertSame(true, tagDAO.update(tag));
	}

	@Test
	public void checkDelete() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("New tag");
		Long tagID = tagDAO.create(tag);
		assertSame(true, tagDAO.deleteById(tagID));
	}
}
