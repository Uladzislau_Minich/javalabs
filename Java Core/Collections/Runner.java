package core.collections;

public class Runner {

	public static void main(String[] args) {
		CustomArrList<String> arr = new CustomArrList<>(14);
		System.out.println(arr.isEmpty());
		arr.add("arr");
		arr.add("asd");
		arr.add("qwer");
		arr.add("str");
		arr.add("str");
		arr.add("str");
		arr.add("arr");
		arr.add("asd");
		arr.add("qwer");
		arr.add("str");
		arr.add("str");
		arr.add("str");
		arr.add("arr");
		arr.add("asd");
		arr.add("qwer");
		arr.add("str");
		arr.add("str");
		arr.add("str");
		arr.add("new elem",3);
		System.out.println(arr.isEmpty());
		System.out.println(arr);
		System.out.println(arr.size());
		System.out.println(arr.get(2));
		arr.delete(2);
		arr.delete(2);
		arr.delete(2);
		arr.delete(2);
		System.out.println(arr);
		System.out.println(arr.size());
	}

}
