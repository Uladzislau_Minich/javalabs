package com.epam.newsadmin.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newscommon.entity.NewsValueObject;

/**
 * Class NewsValidator.
 * 
 * @author Uladzislau
 *
 */
@Component
public class NewsValidator implements Validator{
	/**
	 * Check that class matches NewsValueObject.class or is an child class of it
	 */
	@Override
	public boolean supports(Class<?> type) {
		return NewsValueObject.class.isAssignableFrom(type);
	}
	/**
	 * Validate news's title, short and full text, and presence author
	 */
	@Override
	public void validate(Object arg0, Errors arg1) {
		NewsValueObject newsVO = (NewsValueObject) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.title", "locale.error.add.edit.new.title.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.shortText", "locale.error.add.edit.new.short-text.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.fullText", "locale.error.add.edit.new.full-text.empty");
		if (newsVO.getNews().getTitle().length() > 30) {
			arg1.rejectValue("news.title", "locale.error.add.edit.new.title.overflow");
		}
		if (newsVO.getNews().getShortText().length() > 300) {
			arg1.rejectValue("news.shortText", "locale.error.add.edit.new.short-text.overflow");
		}
		if (newsVO.getNews().getFullText().length() > 3000) {
			arg1.rejectValue("news.fullText", "locale.error.add.edit.new.full-text.overflow");
		}
		if(newsVO.getAuthor()==null){
			arg1.rejectValue("author.authorName", "locale.error.add.edit.author.empty");
		}
	}
}

