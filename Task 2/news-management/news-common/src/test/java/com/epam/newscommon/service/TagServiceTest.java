package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newscommon.dao.TagDAO;
import com.epam.newscommon.entity.Tag;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.TagServiceImpl;

/**
 * Class TagServiceTest. Contains methods for testing working a class
 * TagService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	@Mock
	private TagDAO tagDAOmock;
	@InjectMocks
	private TagServiceImpl tagServiceMock;

	private Tag tag;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		tagServiceMock = mock(TagServiceImpl.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(tagServiceMock.save(tag)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				tag = new Tag();
				tag.setTagId(123L);
				tag.setTagName("tagname");
				return tag.getTagId();
			}
		});
		Long idTag = tagServiceMock.save(tag);
		Long expectedId = new Long(123);
		Assert.assertEquals(expectedId, idTag);
	}

	@Test
	public void checkFindById() throws ServiceException {
		tag = null;
		when(tagServiceMock.findById(123L)).thenReturn(new Tag());
		tag = tagServiceMock.findById(123L);
		Assert.assertNotNull(tag);
	}

	@Test
	public void checkFindAllNewsTag() throws ServiceException {
		Long idNews = 1L;
		when(tagServiceMock.findAllNewsTag(idNews)).then(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> list = new ArrayList<Tag>();
				list.add(new Tag());
				list.add(new Tag());
				list.add(new Tag());
				return list;
			}
		});

		assertSame(3, tagServiceMock.findAllNewsTag(idNews).size());
	}

}
