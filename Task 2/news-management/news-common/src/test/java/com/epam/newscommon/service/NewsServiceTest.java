package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newscommon.dao.impl.NewsDAOImpl;
import com.epam.newscommon.entity.News;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.NewsServiceImpl;

/**
 * Class NewsServiceTest. Contains methods for testing working a class
 * NewsService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
	@Mock
	private NewsDAOImpl newsDAOMock;
	@InjectMocks
	private NewsServiceImpl newsServiceMock;
	private News news;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		newsDAOMock = mock(NewsDAOImpl.class);
		newsServiceMock = mock(NewsServiceImpl.class);
		news = mock(News.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(newsServiceMock.save(news)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				news = new News();
				news.setNewsId(123L);
				news.setTitle("Title");
				news.setFullText("Full text");
				news.setShortText("Short Text");
				return news.getNewsId();
			}
		});
		assertEquals(new Long(123), newsServiceMock.save(news));
	}

	@Test
	public void checkFindById() throws ServiceException {
		when(newsServiceMock.findById(any(Long.class))).then(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = new News();
				news.setNewsId(123L);
				news.setTitle("Title");
				news.setFullText("Full text");
				news.setShortText("Short Text");
				return news;
			}
		});
		news = new News();
		news.setNewsId(123L);
		news.setTitle("Title");
		news.setFullText("Full text");
		news.setShortText("Short Text");
		assertEquals(news, newsServiceMock.findById(123L));
	}

	@Test
	public void checkUpdate() throws ServiceException, DAOException {
		news = new News();
		newsDAOMock.update(news);
		newsDAOMock.update(news);
		verify(newsDAOMock, times(2)).update(news);
	}

	@Test
	public void checkDeleteById() throws ServiceException, DAOException {
		newsDAOMock.deleteById(new Long[]{123L});
		verify(newsDAOMock).deleteById(new Long[]{123L});
	}

	@Test
	public void checkFindAll() throws ServiceException {
		when(newsServiceMock.findNewsPerPage(null,1,3)).then(new Answer<List<News>>() {

			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> list = new ArrayList<>();
				list.add(new News());
				list.add(new News());
				list.add(new News());
				return list;
			}
		});
		List<News> list = newsServiceMock.findNewsPerPage(null,1,3);
		assertSame(3, list.size());
	}
}
