package com.epam.newsadmin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsadmin.utils.TagValidator;
import com.epam.newscommon.entity.Tag;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsValueObjectService;

/**
 * The class TagController.
 * 
 * @author Uladzislau
 *
 */
@Controller
public class TagController {

	@Autowired
	private NewsValueObjectService newsManagementService;
	@Autowired
	private TagValidator validator;

	/**
	 * Forward to add/edit tag's page
	 * 
	 * @param model
	 *            the model object
	 * @return the string of view's name
	 */
	@RequestMapping(value = "/addTag", method = RequestMethod.GET)
	public String viewTags(Model model) {// with pagination
		List<Tag> tags = null;
		try {
			tags = newsManagementService.findAllTags();
			model.addAttribute("tags", tags);
			model.addAttribute("currentMenu", "add-tags-nav");
			model.addAttribute("newTag", new Tag());
			model.addAttribute("editTag", new Tag());
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "tags";
	}

	/**
	 * Save new tag
	 * 
	 * @param newTag
	 *            the tag object
	 * @param model
	 *            the model object
	 * @param result
	 *            the binding result object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/saveNewTag", method = RequestMethod.POST)
	public String saveTag(@ModelAttribute("newTag") Tag newTag, Model model, BindingResult result) {
		validator.validate(newTag, result);
		try {
			if (result.hasErrors()) {
				List<Tag> tags = newsManagementService.findAllTags();
				model.addAttribute("tags", tags);
				model.addAttribute("currentMenu", "add-tags-nav");
				model.addAttribute("newTag", newTag);
				model.addAttribute("editTag", new Tag());
				return "tags";
			}
			newsManagementService.addTag(newTag);
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}

		return "redirect:/addTag";
	}

	/**
	 * Update tag
	 * 
	 * @param editTag
	 *            the tag object
	 * @param model
	 *            the model object
	 * @param result
	 *            the result object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/saveEditTag", method = RequestMethod.POST)
	public String editTag(@ModelAttribute("editTag") Tag editTag, Model model, BindingResult result) {
		validator.validate(editTag, result);
		try {
			if (result.hasErrors()) {
				List<Tag> tags = newsManagementService.findAllTags();
				model.addAttribute("tags", tags);
				model.addAttribute("currentMenu", "add-tags-nav");
				model.addAttribute("newTag", new Tag());
				model.addAttribute("editTag", editTag);
				return "tags";
			}
			newsManagementService.updateTag(editTag);
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/addTag";
	}

	/**
	 * Delete tag
	 * 
	 * @param model
	 *            the model object
	 * @param tagId
	 *            the tag's id
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/deleteTag/{tagId}", method = RequestMethod.GET)
	public String deleteTag(Model model, @PathVariable("tagId") Long tagId) {
		try {
			newsManagementService.deleteTag(new Long[]{tagId});
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/addTag";
	}
}
