package com.epam.newsclient.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsclient.command.ICommand;
import com.epam.newsclient.utils.CommentValidator;
import com.epam.newsclient.utils.ConfigurationManager;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.entity.NewsValueObject;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsValueObjectService;
import com.epam.newscommon.utils.SearchCriteriaObject;

/**
 * Class AddCommentCommand.
 * 
 * @author Uladzislau
 *
 */
public class AddCommentCommand implements ICommand {
	@Autowired
	private NewsValueObjectService service;
	private static final String PARAM_ID_NEWS = "idNews";
	private static final String PARAM_TEXT_COMMENT = "newComment";
	private static final String PARAM_NEWS_VO= "newsVO";
	private static final String PATH_PAGE_SUCCESS = "path.page.news";
	private final static String ID_PREVIOUS_NEWS = "idPreviousNews";
	private final static String ID_NEXT_NEWS = "idNextNews";
	private static final String ATTR_SESSION_SEARCH_CRITERIA = "searchCriteria";


	/**
	 * Add new comment
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String idNews = request.getParameter(PARAM_ID_NEWS);
		String textComment = request.getParameter(PARAM_TEXT_COMMENT);
		SearchCriteriaObject sc = (SearchCriteriaObject) request.getSession()
				.getAttribute(ATTR_SESSION_SEARCH_CRITERIA);
		Comment comment = new Comment();
		NewsValueObject newsVO = null;
		try {
			comment.setNewsId(Long.parseLong(idNews));
			comment.setText(textComment);
			Long currentNewsId = Long.parseLong(idNews);
			newsVO = service.findNews(currentNewsId);
			Long idNextNews = service.findNextNews(newsVO.getNews().getNewsId(),sc);
			Long idPreviousPage = service.findPreviousNews(newsVO.getNews().getNewsId(),sc);
			if (idNextNews != -1 && idPreviousPage != -1) {
				request.setAttribute(ID_PREVIOUS_NEWS, idPreviousPage);
				request.setAttribute(ID_NEXT_NEWS, idNextNews);
			} else if (idNextNews == -1 && idPreviousPage != 0) {
				request.setAttribute(ID_PREVIOUS_NEWS, idPreviousPage);
				request.setAttribute(ID_NEXT_NEWS, currentNewsId);
			} else if (idNextNews != 0 && idPreviousPage == -1) {
				request.setAttribute(ID_PREVIOUS_NEWS, currentNewsId);
				request.setAttribute(ID_NEXT_NEWS, idNextNews);
			}
			String messageError = CommentValidator.validate(comment);
			if (messageError != null) {
				request.setAttribute("message", messageError);
			} else {
				service.addComment(comment);
				newsVO = service.findNews(currentNewsId);
			}
			request.setAttribute(PARAM_NEWS_VO, newsVO);
		} catch (NumberFormatException e) {
			throw new ServiceException("Invalid input data.", e);
		}
		return ConfigurationManager.getProperty(PATH_PAGE_SUCCESS);
	}

}
