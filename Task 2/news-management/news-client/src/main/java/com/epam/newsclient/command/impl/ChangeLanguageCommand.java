package com.epam.newsclient.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsclient.command.ICommand;
import com.epam.newsclient.utils.ConfigurationManager;

/**
 * Class ChangeLanguageCommand.
 * 
 * @author Uladzislau
 *
 */
public class ChangeLanguageCommand implements ICommand {
	private final static String PAR_LOCALE = "locale";
	private static final String PATH_PAGE_SUCCESS = "path.page.index";

	/**
	 * Change language
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String language = request.getParameter(PAR_LOCALE);
		request.getSession().setAttribute(PAR_LOCALE, language);
		return ConfigurationManager.getProperty(PATH_PAGE_SUCCESS);
	}
}
