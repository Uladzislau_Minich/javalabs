package com.epam.newsadmin.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newscommon.entity.Author;

/**
 * Class AuthorValidator.
 * 
 * @author Uladzislau
 *
 */
@Component
public class AuthorValidator implements Validator {
	
	/**
	 * Check that class matches Author.class or is an child class of it
	 */
	@Override
	public boolean supports(Class<?> arg0) {
		return Author.class.isAssignableFrom(arg0);
	}
	
	/**
	 * Validate author name
	 */
	@Override
	public void validate(Object arg0, Errors arg1) {
		Author author = (Author) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "authorName", "locale.error.add.edit.author.empty");
		if (author.getAuthorName().length() > 30) {
			arg1.rejectValue("authorName", "locale.error.add.edit.author.overflow");
		}

	}

}
