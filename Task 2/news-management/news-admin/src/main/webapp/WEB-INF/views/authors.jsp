<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<spring:url value="/saveEditAuthor" var="saveEditAuthorsUrl"
	htmlEscape="true" />
<spring:url value="/saveNewAuthor" var="saveAuthorsUrl"
	htmlEscape="true" />
<div>
	<c:forEach var="author" items="${authors}">
		<spring:url value="/expiredAuthor/${author.authorId}"
			var="expiredAutorsUrl" htmlEscape="true" />
		<sf:form action="${saveEditAuthorUrl}" commandName="authorEdit"
			onsubmit="return validateAuthorName('input-text'+${author.authorId});">
			<div class="author">
				<label><spring:message code="locale.authors.author" /></label>
				<sf:input type="text" path="expired" value="${author.expired}"
					style="display:none;" />
				<sf:input type="text" path="authorId" value="${author.authorId}"
					style="display:none;" />
				<sf:input type="text" path="authorName" value="${author.authorName}"
					class="input-text" id="input-text${author.authorId}" />
				<b id="${author.authorId}" onclick="showButton(${author.authorId})"
					class="edit-button"><spring:message code="locale.authors.edit" /></b>
				<input type="submit" id="update${author.authorId}"
					value="<spring:message code="locale.authors.update"/>"
					class="update" /> <a href="${expiredAutorsUrl}"
					id="expired${author.authorId}" class="expired"><spring:message
						code="locale.authors.expired" /></a> <b id="cancel${author.authorId}"
					class="cancel" onclick="cancel(${author.authorId})"><spring:message
						code="locale.authors.cancel" /></b>
			</div>
			<c:if test="${author.authorId == authorEdit.authorId}">
				<div id="authorForUndisable" style="display: none;">${editTag.tagId}</div>
				<div>
					<sf:errors path="authorName" cssClass="msg" />
				</div>
				<script>
				$(document).ready(function(){
						var id =$('#authorForUndisable').text();
						showButton(id);
					})
				</script>
			</c:if>
		</sf:form>
	</c:forEach>
	<br>
	<sf:form action="${saveAuthorsUrl}" commandName="newAuthor"
		method="POST" onsubmit="return validateAuthorName('input-text');">
		<div class="author">
			<label><spring:message code="locale.authors.add-author" /></label>
			<sf:input type="text" path="authorName" value=""
				class="input-new-author" id="input-text" />
			<input type="submit"
				value="<spring:message	code="locale.authors.save" />" />
		</div>
		<div>
			<sf:errors path="authorName" cssClass="msg" />
		</div>
	</sf:form>
</div>

<script type="text/javascript">
	doDisabled();
</script>