package com.epam.newscommon.dao;

import java.util.List;

import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.utils.SearchCriteriaObject;
import com.epam.newscommon.entity.News;


/**
 * Interface NewsDAO. Classes which implements this interface contain action
 * with table "NEWS" in the database
 * 
 * @author Uladzislau_Minich
 *
 */
public interface NewsDAO extends BaseDAO<News> {
	 /**
		 * Find list of news
		 *
		 * @return list of objects news
		 * @throws DAOException
		 */
	List<News> findNewsPerPage(SearchCriteriaObject sc, int startIndex, int endIndex) throws DAOException;

	/**
	 * Add information to table news_author
	 * 
	 * @param newsId
	 * @param authorId
	 * @return
	 * @throws DAOException
	 */
	void addNewsAuthors(Long newsId, Long authorId) throws DAOException;

	/**
	 * Delete information from table news_author
	 * 
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	void deleteNewsAuthor(Long[] newsId) throws DAOException;

	/**
	 * Add information to table news_tag
	 * 
	 * @param newsId
	 * @return
	 * @throws DAOException
	 */
	void addNewsTag(Long tagId, Long newsId) throws DAOException;

	/**
	 * Delete information from table news_tag
	 * 
	 * @param idNews
	 * @return
	 * @throws DAOException
	 */
	void deleteNewsTag(Long[] idNews) throws DAOException;

	/**
	 * Find all news some authors
	 * 
	 * @param authorId
	 * @return list of news
	 * @throws DAOException
	 */
	List<News> findAllAuthorsNews(Long authorId) throws DAOException;

	/**
	 * Count news
	 * 
	 * @return amount of news
	 * @throws DAOException
	 */
	int countNews(SearchCriteriaObject sc) throws DAOException;
	
	/**
	 * Find news on the list, which is before the current
	 * 
	 * @param currentNewsId
	 * @return news's id
	 * @throws DAOException
	 */
	Long findPreviousNews(Long currentNewsId, SearchCriteriaObject sc) throws DAOException;

	/**
	 * Find news on the list, which is following the current
	 * 
	 * @param currentNewsId
	 * @return news's id
	 * @throws DAOException
	 */
	Long findNextNews(Long currentNewsId, SearchCriteriaObject sc) throws DAOException;
}
