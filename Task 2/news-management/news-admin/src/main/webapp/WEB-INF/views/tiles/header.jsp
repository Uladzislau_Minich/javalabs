<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div>
	<div class="header-title">
		<spring:message code="locale.header.title" />
	</div>
	<div class="hello">
		<security:authorize access="isAuthenticated()">
			<security:authentication property="principal.username" />

			<c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="GET">
				<button type="submit">
					<spring:message code="locale.header.logout-button" />
				</button>
			</form>
		</security:authorize>
	</div>



	<span class="locale"> <a href="?lang=en">ENG</a> | <a
		href="?lang=ru">RU</a>
	</span>
</div>

