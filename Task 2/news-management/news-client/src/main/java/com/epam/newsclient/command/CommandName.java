package com.epam.newsclient.command;

/**
 * Enumeration CommandName. Contain names of command.
 * 
 * @author Uladzislau
 *
 */
public enum CommandName {
	SHOW_LIST_NEWS, CHANGE_LANGUAGE, VIEW_NEWS, ADD_COMMENT
}