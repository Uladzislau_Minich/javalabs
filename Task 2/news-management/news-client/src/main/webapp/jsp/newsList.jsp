<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css" type="text/css">
	
	<script src="js/myapi.js"></script>
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.2.custom.min.js"></script>
    <script src="js/ui.dropdownchecklist-1.5-min.js"></script>
	
<c:if test="${sessionScope.locale == 'en' or empty sessionScope.locale}">
	<fmt:setBundle basename="message_EN" />
</c:if>
<c:if test="${sessionScope.locale == 'ru'}">
	<fmt:setBundle basename="message_RU" />
</c:if>

<title><fmt:message key="newslist.title" /></title>
</head>

<body link="blue" vlink="blue" alink="blue" >
	<%@include file="/jsp/elements/header.jspf"%>
	<br/>
	<div id="maincontent">
	<div align="center">
	<form action="Controller" method="GET">
		<select id="authorSelect" name="authorSelect">
			<option selected="selected" disabled="disabled">...</option>
			<c:forEach var="author" items="${authors}">
					<option value="${author.authorId}"><c:out value="${author.authorName}"/></option>
			</c:forEach>
		</select>
		<select id="multiSelect" multiple="multiple" name="tagsSelect">
			<c:forEach var="tag" items="${tags}">
				<option value="${tag.tagId}"><c:out value="${tag.tagName}"/></option>
			</c:forEach>
		</select> 
		<input type="hidden" name="command" value="show_list_news" />
		<input type="hidden" name="indexNewsPage" value="1"/>
		<input type="submit" value="<fmt:message key="newslist.filter.button" />"/>
		</form>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#multiSelect').change(function() { 
			console.log($(this).val()); 
			}).dropdownchecklist( { emptyText: "...", width: 150 } )
		})
		</script> 		 
		<form action="Controller" method = "GET">
			<input type=hidden name="command" value="show_list_news"/>
  		 	<input type="submit" value="<fmt:message key="newslist.reset-filter.button" />"/>
  		 	<input type="hidden" name="indexNewsPage" value="1"/>
  		 	<input type="hidden" name="resetFilter" value="true"/>
		</form>
	</div>
	<br/>
	<div>
	<c:choose>
		<c:when test="${not empty newsList }">
			<c:forEach var="newsVO" items="${newsList}">
				<div id="newsTitle"><c:out value="${newsVO.news.title}"/></div>
				<div id="newsAuthor"><c:out value="( by ${newsVO.author.authorName} )"/> </div>
				<div id="newsModificationDate"><c:out value="${newsVO.news.modificationDate}"/></div>
				<br/>
				<br/>
				<div id="newsShortText"><c:out value="${newsVO.news.shortText}"/></div>
				<c:if test="${not empty newsVO.tags }">
					<c:forEach var="tag" items="${newsVO.tags}">
						<div id="newsTags">#<c:out value="${tag.tagName}"/></div>
					</c:forEach>
				</c:if>
				<br/>
				<c:choose>
					<c:when test="${not empty newsVO.comments }">
						<div id="newsComments" >
							<fmt:message key="newslist.comments" />(<c:out value="${fn:length(newsVO.comments)}" />)</div>
					</c:when>
					<c:otherwise>
						<div id="newsComments">
							<fmt:message key="newslist.comments" />(0)</div>
					</c:otherwise>
				</c:choose>
				<div id="newsShowMore">
				<a href="controller?command=view_news&news_id=${newsVO.news.newsId}&indexPreviousPage=${sessionScope.idPreviousNewsPage}"><fmt:message
						key="newslist.view" /></a></div>
				<br/>
				<hr />
				<br/>
			</c:forEach>
		</c:when>
		<c:otherwise>
		<br><br>
		<div align="center">
			<fmt:message key="message.newslist.no-found-news" />
		</div>
		</c:otherwise>
		</c:choose>
	</div>
	<div  align="center" id="paganition" >
	<ctg:pagination-page countNews="${countNews}" currentPage="${indexNewsPage}"/>
	</div>
	</div>
	<div align="center">
		<%@include file="/jsp/elements/footer.jspf"%>
	</div>
</body>
</html>