import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class MyClassLoader extends ClassLoader {

	public MyClassLoader(ClassLoader parent) {
		super(parent);
	}

	public Class loadClass(String name) throws ClassNotFoundException {
		if (!"com.epam.news.entity.Author".equals(name))
			return super.loadClass(name);

		try {
			String url = "file:D:/JAVALAB/workspaceMars/newsportal/target/classes/"
					+ "com/epam/news/entity/Author.class";
			URL myUrl = new URL(url);
			URLConnection connection = myUrl.openConnection();
			InputStream input = connection.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int data = input.read();

			while (data != -1) {
				buffer.write(data);
				data = input.read();
			}
			input.close();
			byte[] classData = buffer.toByteArray();
			return defineClass("com.epam.news.entity.Author", classData, 0, classData.length);

		} catch (MalformedURLException e) {
			System.out.println("Error. URL exception.");
		} catch (IOException e) {
			System.out.println("Error. I/O exception.");
		}

		return null;
	}

}

public class CustomClassLoaderRunner {

	@SuppressWarnings("rawtypes")
	public static void main(String[] args)
			throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		ClassLoader parentClassLoader = MyClassLoader.class.getClassLoader();
		MyClassLoader classLoader = new MyClassLoader(parentClassLoader);
		Class myObjectClass = classLoader.loadClass("com.epam.news.entity.Author");
		System.out.println(myObjectClass.getName());
		Object obj = myObjectClass.newInstance();
		System.out.println(obj);// Console: Author [authorId=null, authorName=null, expired=null]
	}
}
