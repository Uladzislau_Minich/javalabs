package com.epam.newsclient.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Class CommandFactory.
 * 
 * @author Uladzislau
 *
 */
public class CommandFactory {
	private static Logger log = Logger.getLogger(CommandFactory.class);
	private final static String PAR_COMMAND = "command";

	@Autowired
	@Qualifier("ShowListNewsCommand")
	private ICommand showListNewsCommand;
	// private ShowListNewsCommand showListNewsCommand;
	@Autowired
	@Qualifier("ChangeLanguageCommand")
	private ICommand changeLanguageCommand;
	@Autowired
	@Qualifier("ViewNewsCommand")
	private ICommand viewNewsCommand;
	@Autowired
	@Qualifier("AddCommentCommand")
	private ICommand addCommentCommand;

	/**
	 * Define which command must be execute and create it
	 * 
	 * @param request
	 *            the http servlet request object
	 * @return the ICommand object
	 */
	public ICommand defineCommand(HttpServletRequest request) {
		ICommand currentCommand = null;
		String action = request.getParameter(PAR_COMMAND);
		if (action == null || action.isEmpty()) {
			return currentCommand;
		}
		try {
			CommandName currentEnum = CommandName.valueOf(action.toUpperCase());
			switch (currentEnum) {
			case SHOW_LIST_NEWS:
				currentCommand = showListNewsCommand;
				break;
			case CHANGE_LANGUAGE:
				currentCommand = changeLanguageCommand;
				break;
			case VIEW_NEWS:
				currentCommand = viewNewsCommand;
				break;
			case ADD_COMMENT:
				currentCommand = addCommentCommand;
				break;
			default:
				throw new EnumConstantNotPresentException(null, action);
			}
		} catch (IllegalArgumentException | EnumConstantNotPresentException e) {
			log.error("No such command: " + action, e);
		}
		return currentCommand;
	}
}
