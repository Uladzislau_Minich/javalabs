package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;
/**
 * Interface CommentService. Classes which implements this interface contain
 * action with CommentDAO in DAO layer
 * 
 * @author Uladzislau_Minich
 *
 */
public interface CommentService extends BaseService<Comment>{
	/**
	 * Find all news's comments
	 * 
	 * @return list of objects Comment
	 * @throws ServiceException
	 */
	List<Comment> showAllNewsComment(Long newsId) throws ServiceException;
}
