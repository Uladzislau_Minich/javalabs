package com.epam.news.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.implementation.NewsValueObjectServiceImpl;

/**
 * Class NewsValueObjectTest. Contains methods for testing working a class
 * NewsValueObjectService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsValueObjectTest {
	@Mock
	private NewsService newsServiceMock;
	@Mock
	private AuthorService authorServiceMock;
	@Mock
	private TagService tagServiceMock;
	@Mock
	private CommentService commentServiceMock;
	@InjectMocks
	private NewsValueObjectServiceImpl service;

	private News news;
	private Author author;
	private List<Tag> tags;
	private List<Comment> comments;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		service = mock(NewsValueObjectServiceImpl.class);
		News news = new News();
		news.setNewsId(1L);
		news.setTitle("Title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		news.setCreationTime(new Timestamp(System.currentTimeMillis()));
		news.setModificationDate(new Timestamp(System.currentTimeMillis()));
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Author name");
		author.setExpired(null);
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("Tag name1");
		Tag tag2 = new Tag();
		tag2.setTagId(2L);
		tag2.setTagName("Tag name2");
		List<Tag> tags = new ArrayList<>();
		tags.add(tag);
		tags.add(tag2);
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setText("Comment text 1");
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		comments = new ArrayList<>();
		comments.addAll(comments);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void checkSaveNews() throws ServiceException {
		when(service.saveNews(any(News.class), any(Author.class), any(ArrayList.class)))
				.then(new Answer<NewsValueObject>() {

					@Override
					public NewsValueObject answer(InvocationOnMock invocation) throws Throwable {
						NewsValueObject newsVO = new NewsValueObject();
						newsVO.setNews(news);
						return newsVO;
					}

				});
		assertEquals(news, service.saveNews(news, author, tags).getNews());
	}

	@Test
	public void checkDeleteNews() throws ServiceException {
		newsServiceMock.deleteNewsAuthor(1L);
		newsServiceMock.deleteNewsTag(1L);
		newsServiceMock.deleteById(1L);
		verify(newsServiceMock).deleteNewsAuthor(1L);
		verify(newsServiceMock).deleteNewsTag(1L);
		verify(newsServiceMock).deleteById(1L);
	}

	@Test
	public void checkFindNews() throws ServiceException {
		when(service.findNews(any(Long.class))).thenReturn(new NewsValueObject());
		assertNotNull(service.findNews(1L));
	}

	@Test
	public void checkUpdate() throws ServiceException {
		when(service.updateNews(any(NewsValueObject.class))).then(new Answer<NewsValueObject>() {
			@Override
			public NewsValueObject answer(InvocationOnMock invocation) throws Throwable {
				NewsValueObject newsVO = new NewsValueObject();
				newsVO.setNews(new News());
				return newsVO;
			}

		});
		NewsValueObject newsVO = new NewsValueObject();
		newsVO.setNews(news);
		newsVO = service.updateNews(newsVO);
		Assert.assertNotEquals(news, newsVO.getNews());
	}

	@Test
	public void checkGetCurrentAuthors() throws ServiceException {
		when(service.getCurrentAuthors()).thenReturn(new ArrayList<Author>());
		assertNotNull(service.getCurrentAuthors());
	}

	@Test
	public void checkExpiredAuthor() throws ServiceException {
		service.expiredAuthor(author);
		verify(service).expiredAuthor(author);
	}

	@Test
	public void checkAddComment() throws ServiceException {
		service.addComment(new Comment());
		verify(service).addComment(new Comment());

	}

	@Test
	public void checkDeleteComment() throws ServiceException {
		service.deleteComment(1L);
		verify(service).deleteComment(1L);
	}

	@Test
	public void checkSaveAuthor() throws ServiceException {
		when(authorServiceMock.save(any(Author.class))).thenReturn(new Long(1));
		assertEquals(new Long(1), authorServiceMock.save(author));
	}

	@Test
	public void checkDeleteAuthor() throws ServiceException {
		service.deleteAuthor(1L);
		verify(service).deleteAuthor(1L);
	}

	@Test
	public void checkFindAllTag() throws ServiceException {
		when(service.findAllTags()).then(new Answer<List<Tag>>() {

			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				return new ArrayList<Tag>();
			}
		});
		assertSame(0, service.findAllTags().size());
	}
}
