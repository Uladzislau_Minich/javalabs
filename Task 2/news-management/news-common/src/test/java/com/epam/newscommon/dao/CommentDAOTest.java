package com.epam.newscommon.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.dao.CommentDAO;
import com.epam.newscommon.dao.impl.CommentDAOImpl;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * Class CommentDAOTest. Contains methods for testing working a class TagDAO.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-context-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDAOTest {
	@Autowired
	private CommentDAO commentDAO;

	/**
	 * Load data to database.
	 * 
	 * @throws FileNotFoundException
	 * @throws DatabaseUnitException
	 * @throws SQLException
	 */
	@Before
	public void doSetup() throws FileNotFoundException, DatabaseUnitException, SQLException {
		Locale.setDefault(Locale.ENGLISH);
		Connection con = CommentDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();

		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-content.xml")), };

		DatabaseOperation.CLEAN_INSERT.execute(
				new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new CompositeDataSet(datasets));
		con.close();
	}

	/**
	 * Clean database after testing
	 * 
	 * @throws DataSetException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 */
	@After
	public void cleanDB() throws DataSetException, FileNotFoundException, SQLException, DatabaseUnitException {
		Connection con = CommentDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		DatabaseOperation.DELETE_ALL.execute(new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete-content.xml")));
		con.close();
	}

	@Test
	public void checkCreateAndDelete() throws DAOException {
		Comment comment = new Comment();
		comment.setNewsId(4L);
		comment.setText("Comment text");
		comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
		Long unexpectedCommentId = -1L;
		Long actualCommentId = commentDAO.create(comment);
		Assert.assertNotEquals(unexpectedCommentId, actualCommentId);
		commentDAO.deleteById(new Long[]{actualCommentId});
		comment = commentDAO.findById(actualCommentId);
		assertSame(null, comment);
	}

	@Test
	public void checkFindById() throws DAOException {
		Comment comment = commentDAO.findById(1L);
		Assert.assertNotNull(comment);
	}

	@Test
	public void checkUpdate() throws DAOException {
		Comment comment = new Comment();
		comment.setNewsId(1L);
		comment.setText("Comment text");
		comment.setCommentId(1L);
		comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
		commentDAO.update(comment);
		Assert.assertEquals("Comment text", commentDAO.findById(1L).getText());
	}
	
	@Test
	public void checkFindAllNewsComment() throws DAOException {
		List<Comment> list = commentDAO.findAllNewsComment(1L);
		Assert.assertEquals(2, list.size());
	}

}
