package com.epam.newscommon.service.impl;

import java.util.List;

import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.dao.CommentDAO;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.service.CommentService;

/**
 * Class CommentServiceImpl. Implementation of the CommentService
 * 
 * @author Uladzislau_Minich
 *
 */
public class CommentServiceImpl implements CommentService {
	private CommentDAO commentDAO;

	/**
	 * @see {@link com.epam.newscommon.service.CommentService#save()}
	 */
	@Override
	public Long save(Comment entity) throws ServiceException {
		try {
			entity.setCommentId(commentDAO.create(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity.getCommentId();
	}

	/**
	 * @see {@link com.epam.newscommon.service.CommentService#findById()}
	 */
	@Override
	public Comment findById(Long id) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDAO.findById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return comment;

	}

	/**
	 * @see {@link com.epam.newscommon.service.CommentService#update()}
	 */
	@Override
	public void update(Comment entity) throws ServiceException {
		try {
			commentDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.CommentService#deleteById()}
	 */
	@Override
	public void deleteById(Long[] ids) throws ServiceException {
		try {
			commentDAO.deleteById(ids);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.CommentService#showAllNewsComment()}
	 */
	@Override
	public List<Comment> findAllNewsComment(Long id) throws ServiceException {
		List<Comment> comments = null;
		try {
			comments = commentDAO.findAllNewsComment(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return comments;
	}
	/**
	 * @see {@link com.epam.newscommon.service.CommentService#deleteCommentsByNewsId(Long[])}
	 */
	@Override
	public void deleteCommentsByNewsId(Long[] newsIds) throws ServiceException {
		try{
			commentDAO.deleteCommentByNewsId(newsIds);
		}catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * Set a field commentDAO
	 * 
	 * @param commentDAO
	 */
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	

}
