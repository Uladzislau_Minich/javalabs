<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<spring:url value="/addComment/${newsVO.news.newsId}"
	var="addCommentUrl" htmlEscape="true" />
<spring:url value="/nextNews/${newsVO.news.newsId}" var="nextNewstUrl"
	htmlEscape="true" />
<spring:url value="/previousNews/${newsVO.news.newsId}"
	var="prevNewsUrl" htmlEscape="true" />
<c:out value=""/>
<div class="wrapper">
	<div class="news-title"><c:out value="${newsVO.news.title }"/></div>
	<span class="news-author"><c:out value="(by ${newsVO.author.authorName })"/></span> <span
		class="news-modification-date"><c:out value="${newsVO.news.modificationDate }"/></span><br>
	<br>
	<div class="news-full-text"><c:out value="${newsVO.news.fullText }"/></div>
	<br>
	<div class="comments">
		<c:forEach var="comment" items="${newsVO.comments }">
			<spring:url
				value="/deleteComment/${comment.commentId}&${newsVO.news.newsId}"
				var="deleteCommentUrl" htmlEscape="true" />
			<div class="commentCreationDate"><c:out value="${comment.creationDate }"/></div>
			<form action="${deleteCommentUrl}" method="POST">
			<a href="${deleteCommentUrl}" class="deleteComment">X</a>
			</form>
			<div class="commentText"><c:out value="${comment.text}"/></div>
			<br>
		</c:forEach>
		<sf:form class="new-comment" action="${addCommentUrl}"
			commandName="commentNews" onSubmit="return validationComment()">
			<sf:textarea path="text" id="new-comment-text"></sf:textarea>
			<input type="submit" id="new-comment-button"
				value="<spring:message code="locale.news.post-comment"/>">
				<div><sf:errors path="text" cssClass="msg" /></div>
		</sf:form>
	</div>
	<div>
		<a href="${prevNewsUrl}" id="prev-news-button"><spring:message
				code="locale.news.show-previous" /></a> <a href="${nextNewstUrl}"
			id="next-news-button"><spring:message
				code="locale.news.show-next" /></a>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#new-comment-text').keyup(function() {
			$('.msg').empty();
			$('#new-comment-text').removeClass('error');
		})
	})
</script> 