package core;

public class LoadingClass {
	
	public static void main(String[] args){
		System.out.println("A a:");//Class not loaded
		A a;
		System.out.println("A a=new A():");//Class loaded
		A a2 = new A();
		
	}
}
class A{
	
	static{
		System.out.println("A loaded.");
	}
	int a;
}
