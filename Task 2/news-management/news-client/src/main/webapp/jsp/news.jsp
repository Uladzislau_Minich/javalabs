<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css" type="text/css">
	<script src="js/jquery.js"></script>
	<script src="js/myapi.js"></script>
<c:if test="${sessionScope.locale == 'en' or empty sessionScope.locale}">
	<fmt:setBundle basename="message_EN" />
</c:if>
<c:if test="${sessionScope.locale == 'ru'}">
	<fmt:setBundle basename="message_RU" />
</c:if>
<title><fmt:message key="news.title" /></title>
</head>
<body link="blue" vlink="blue" alink="blue" >
	<div align="center">
		<%@include file="/jsp/elements/header.jspf"%>
	</div>
	<div id="maincontent">
		<div id="back">
			<a  href="Controller/?command=show_list_news&indexNewsPage=${sessionScope.idPreviousNewsPage}">
			<fmt:message key="news.back" />
			</a>
		</div>
		<br />
		<br />
		<c:out value=""></c:out>
		<div id="newsTitle"><c:out value="${newsVO.news.title}"/></div>
		<div id="newsAuthor"><c:out value="( by ${newsVO.author.authorName} )"/></div>
		<div id="newsModificationDate"><c:out value="${newsVO.news.modificationDate}"/></div>
		<br /> <br />
		<div id="newsShortText"><c:out value="${newsVO.news.shortText}"/></div>
		<br />
		<div id="newsFullText"><c:out value="${newsVO.news.fullText}"/></div>
		<br>
		<c:if test="${not empty newsVO.comments }">
			<c:forEach var="comment" items="${newsVO.comments}">
			<div class="comment">
					<span id="commentCreationDate"><c:out value="${comment.creationDate}"/></span><br>
					<div id="commentText"><c:out value="${comment.text}"/></div>
				</div>
			<br>
			</c:forEach>
		</c:if>
		 <form action="Controller" method="GET" onSubmit="return validationComment()">
			<textarea id="textareaNewComment" name="newComment"></textarea>
			<br /> <br /> <br /> <br /> <br /> 
			<input id="addComment"
				type="submit" value="<fmt:message key="news.add.comment" />" />
				<input type="hidden" name="command" value="add_comment"/>
				<input type="hidden" name="idNews" value="${newsVO.news.newsId}"/>
		</form>
		<script type="text/javascript">
			$(document).ready(function(){
				
				$('#textareaNewComment').keyup(function(){
					$('.msg').empty();
					$('#textareaNewComment').removeClass('error');	
				})
			})	
			function validationComment() {
				var a = $('#textareaNewComment').val();
				var flag = true;
				$('.msg').empty();
				$('#textareaNewComment').removeClass('error');
				if (a.trim() == '') {
					$('#textareaNewComment').addClass('error');
					$('#addComment').before('<div class="msg"> <fmt:message key="news.add.comment.error.empty" /></div>');
					flag=false;
				}
				if (a.trim().length > 300) {
					$('#textareaNewComment').addClass('error');
					$('#addComment').before('<div class="msg"><fmt:message key="news.add.comment.error.overflow" /></div>');
					flag=false;	
				}
				return flag;
			}
		</script>
		<br/>
		<br/>
		<div id="previous">
			<a href="Controller?command=view_news&news_id=${idPreviousNews}&indexNewsPage=${sessionScope.indexPreviousPage}">
				<fmt:message key="news.previous" />
			</a>
		</div>
		<div id="next">
			<a href="Controller?command=view_news&news_id=${idNextNews}&indexNewsPage=${sessionScope.indexPreviousPage}">
				<fmt:message key="news.next" />
			</a>
		</div>
		
	</div>
	<div align="center">
		<%@include file="/jsp/elements/footer.jspf"%>
	</div>
</body>
</html>