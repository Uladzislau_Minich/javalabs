package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * Interface CommentDAO. Classes which implements this interface contain action
 * with table "COMMENTS" in the database
 * 
 * @author Uladzislau_Minich
 *
 */
public interface CommentDAO extends BaseDAO<Comment> {
	/**
	 * Find all news's comments
	 * 
	 * @return list of objects Comment
	 * @throws DAOException
	 */
	List<Comment> showAllNewsComment(Long newsId) throws DAOException;
}
