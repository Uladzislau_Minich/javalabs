package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * Interface TagService. Classes which implements this interface contain action
 * with TagDAO in DAO layer
 * 
 * @author Uladzislau_Minich
 *
 */
public interface TagService extends BaseService<Tag> {
	/**
	 * Find all tags which contain some news
	 * 
	 * @param newsId
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> findAllNewsTag(Long newsId) throws ServiceException;

	/**
	 * Find all tags
	 * 
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> findAllTag() throws ServiceException;
}
