package com.epam.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newscommon.constant.DBColumnName;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.dao.CommentDAO;
import com.epam.newscommon.entity.Comment;

/**
 * Class CommentDAOImpl. Implementation of CommentDAO
 * 
 * @author Uladzislau_Minich
 *
 */
public class CommentDAOImpl implements CommentDAO {
	private static BasicDataSource dataSource;
	private static final String SQL_FIND_ALL_NEWS_COMMENTS = "Select * from comments where news_id=? ORDER BY CREATION_DATE DESC";
	private static final String SQL_FIND_BY_ID = "Select * from comments where comments.comment_id=?";
	private static final String SQL_UPDATE_COMMENT = "Update comments SET news_id=?,comment_text=?,creation_date=? where comment_id=?";
	private static final String SQL_CREATE_COMMENT = "Insert into comments(comment_id,news_id,comment_text,creation_date) values(COMMENTS_SEQ.nextval,?,?,?)";
	private static final String SQL_DELETE_COMMENT = "Delete from comments where comments.comment_id=?";
	private static final String SQL_DELETE_COMMENT_BY_NEWS_IDS = "Delete from comments where comments.news_id=?";

	/**
	 * @see {@link com.epam.newscommon.dao.CommentDAO#create(Comment)}
	 */
	@Override
	public Long create(Comment entity) throws DAOException {
		Long id = (long) 0;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_COMMENT,
				new String[] { DBColumnName.COMMENT_ID })) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getText());
			preparedStatement.setTimestamp(3, new Timestamp(new java.util.Date().getTime()));
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()) {
				id = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return id;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.CommentDAO#findById(int)}
	 */
	@Override
	public Comment findById(Long id) throws DAOException {
		Comment comment = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				comment = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comment;
	}

	/**
	 * @see {@link com.epam.newscommon.dao.CommentDAO#update(Comment)}
	 */
	@Override
	public void update(Comment entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT)) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getText());
			preparedStatement.setTimestamp(3, entity.getCreationDate());
			preparedStatement.setLong(4, entity.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.CommentDAO#deleteById(int)}
	 */
	@Override
	public void deleteById(Long[] ids) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT)) {
			for (Long id : ids) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.dao.CommentDAO#showAllNewsComment(int)}
	 */
	@Override
	public List<Comment> findAllNewsComment(Long id) throws DAOException {
		List<Comment> allComments = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_NEWS_COMMENTS)) {
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Comment comment = parseResultSet(resultSet);
				allComments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return allComments;
	}
	@Override
	public void deleteCommentByNewsId(Long[] newsIds) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_NEWS_IDS)) {
			for (Long id : newsIds) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	/**
	 * Parse result set object
	 * 
	 * @param resultSet
	 *            the result set object
	 * @return the comment object
	 * @throws SQLException
	 */
	private Comment parseResultSet(ResultSet resultSet) throws SQLException {
		Comment comment = new Comment();
		comment.setCommentId(resultSet.getLong(DBColumnName.COMMENT_ID));
		comment.setNewsId(resultSet.getLong(DBColumnName.NEWS_ID));
		comment.setText(resultSet.getString(DBColumnName.COMMENT_TEXT));
		comment.setCreationDate(resultSet.getTimestamp(DBColumnName.COMMENT_CREATION_DATE));
		return comment;
	}

	/**
	 * Set a field dataSource
	 * 
	 * @param dataSource
	 */
	public static void setDataSource(BasicDataSource dataSource) {
		CommentDAOImpl.dataSource = dataSource;
	}

	/**
	 * Get a dataSource
	 * 
	 * @return
	 */
	public static BasicDataSource getDataSource() {
		return dataSource;
	}


}
