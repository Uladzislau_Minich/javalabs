package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

/**
 * Interface AuthorService. Classes which implements this interface contain
 * action with AuthorDAO in DAO layer
 * 
 * @author Uladzislau_Minich
 *
 */
public interface AuthorService extends BaseService<Author> {
	/**
	 * Find Author by author name
	 * 
	 * @param authorName
	 * @return author 
	 * @throws ServiceException
	 */
	Author findAuthorByName(String authorName) throws ServiceException;

	/**
	 * Expires author
	 * 
	 * @param idAuthor
	 * @return	
	 * @throws ServiceException
	 */
	boolean expiredAuthor(Long idAuthor) throws ServiceException;

	/**
	 * Find news's author
	 * 
	 * @param idNews
	 * @return author 
	 * @throws ServiceException
	 */
	Author findAuthorByNews(Long idNews) throws ServiceException;

	/**
	 * Find actual authors
	 * 
	 * @return list of authors
	 * @throws ServiceException
	 */
	List<Author> findNotExpiredAuthors() throws ServiceException;
}
