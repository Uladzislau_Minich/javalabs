<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>


<c:url value="/j_spring_security_check" var="loginUrl" />

<c:if test="${not empty error}">
	<div class="error">
		<spring:message code="locale.error.login.password.incorrect" />
	</div>
</c:if>
<form name='loginForm' action="${loginUrl}" method='POST'>
	<table>
		<tr>
			<td><spring:message code="locale.login.login" /></td>
			<td><input type='text' name='username' value=''></td>
		</tr>
		<tr>
			<td><spring:message code="locale.login.password" /></td>
			<td><input type='password' name='password' /></td>
		</tr>
		<tr>
			<td ><input name="submit" type="submit" 
				value="<spring:message code="locale.login.login-button"/>"
				id="button-save-news"/></td>
		</tr>
	</table>

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

</form>

</body>
</html>