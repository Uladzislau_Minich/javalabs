package com.epam.news.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.implementation.CommentDAOImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.implementation.CommentServiceImpl;

/**
 * Class CommentServiceTest. Contains methods for testing working a class
 * CommentService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	@Mock
	private CommentDAOImpl commentDAOMock;
	@InjectMocks
	private CommentServiceImpl commentServiceMock;

	/**
	 * Initialize objects
	 */
	@Before
	public void setup() {
		commentDAOMock = mock(CommentDAOImpl.class);
		commentServiceMock = mock(CommentServiceImpl.class);
	}

	@Test
	public void checkCallMethod() throws DAOException, ServiceException {
		Comment comment = new Comment();
		commentDAOMock.create(comment);
		commentDAOMock.deleteById(1L);
		commentDAOMock.findById(1L);
		commentDAOMock.update(comment);
		commentDAOMock.showAllNewsComment(1L);
		commentDAOMock.showAllNewsComment(1L);
		verify(commentDAOMock).create(comment);
		verify(commentDAOMock, times(2)).showAllNewsComment(1L);
		verify(commentDAOMock).deleteById(1L);
		verify(commentDAOMock).findById(1L);
		verify(commentDAOMock).update(comment);

	}

	@Test
	public void checkFindById() throws ServiceException {
		Comment comment = new Comment();
		Long id = 123L;
		comment.setCommentId(id);
		when(commentServiceMock.findById(123L)).thenReturn(comment);
		assertSame(id, commentServiceMock.findById(123L).getCommentId());
	}

	@Test
	public void checkSave() throws ServiceException {
		Comment comment = new Comment();
		comment.setCommentId(123L);
		when(commentServiceMock.save(comment)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				Comment com = new Comment();
				com.setCommentId(123L);
				com.setText("Comment text");
				com.setCreationDate(new Timestamp(new java.util.Date().getTime()));
				return com.getCommentId();
			}

		});
		assertSame(comment.getCommentId(), commentServiceMock.save(comment));
	}

	@Test
	public void checkUpdate() throws ServiceException {
		final Comment comment = new Comment();
		comment.setCommentId(123L);
		comment.setNewsId(1L);
		when(commentServiceMock.update(comment)).then(new Answer<Boolean>() {
			@Override
			public Boolean answer(InvocationOnMock invocation) throws Throwable {
				comment.setNewsId(2L);
				if (comment.getNewsId() == 2) {
					return true;
				}
				return false;
			}

		});
		assertSame(true, commentServiceMock.update(comment));
	}

	@Test(expected = ServiceException.class)
	public void checkCreate() throws ServiceException {
		when(commentServiceMock.save(null)).thenThrow(new ServiceException());
		assertSame(true, commentServiceMock.save(null));
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void checkShowAllNewsComment() throws ServiceException {
		Long idNews = 1L;
		when(commentServiceMock.showAllNewsComment(idNews)).then(new Answer() {
			@Override
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> list = new ArrayList<Comment>();
				list.add(new Comment());
				list.add(new Comment());
				list.add(new Comment());
				return list;
			}
		});
		assertSame(3, commentServiceMock.showAllNewsComment(idNews).size());
	}
}
