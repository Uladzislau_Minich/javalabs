<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<spring:url value="/saveEditTag" var="saveEditTagUrl" htmlEscape="true" />
<spring:url value="/saveNewTag" var="saveNewTagUrl" htmlEscape="true" />

<div>
	<c:forEach var="tag" items="${tags}">
		<spring:url value="/deleteTag/${tag.tagId}" var="deleteTagUrl"
			htmlEscape="true" />
		<sf:form action="${saveEditTagUrl}" commandName="editTag"
			method="POST"
			onsubmit="return validateTagName('input-text'+${tag.tagId});">
			<div class="tag">
				<label><spring:message code="locale.tags.tag" /></label>
				<sf:input type="text" path="tagId" value="${tag.tagId}"
					style="display:none;" />
				<sf:input type="text" path="tagName" value="${tag.tagName}"
					class="input-text" id="input-text${tag.tagId}" />
				<b id="${tag.tagId}" onclick="showButton(${tag.tagId})"
					class="edit-button"><spring:message code="locale.tags.edit" /></b>
				<input type="submit" id="update${tag.tagId}"
					value="<spring:message code="locale.tags.update"/>" class="update">
				<a href="${deleteTagUrl}" id="delete${tag.tagId}" class="delete"><spring:message
						code="locale.tags.delete" /></a> <b id="cancel${tag.tagId}"
					class="cancel" onclick="cancel(${tag.tagId})"><spring:message
						code="locale.tags.cancel" /></b>
			</div>
			<c:if test="${tag.tagId == editTag.tagId }">
				<div id="tagForUndisable" style="display: none;">${editTag.tagId}</div>
				<div>
					<sf:errors path="tagName" cssClass="msg" />
				</div>
				<script>
				$(document).ready(function(){
						console.log(id);
						var id =$('#tagForUndisable').text();
						console.log(id);
						showButton(id);
					})
				</script>

			</c:if>
		</sf:form>
	</c:forEach>
	<br>
	<sf:form action="${saveNewTagUrl}" commandName="newTag" method="POST"
		onsubmit="return validateTagName('input-text');">
		<div class="tag">
			<label><spring:message code="locale.tags.add-tag" /></label>
			<sf:input type="text" path="tagName" value="" class="input-new-tag"
				id="input-text" />
			<input type="submit"
				value="<spring:message	code="locale.tags.save" />" />
		</div>
		<div id="errorEditTag">
			<sf:errors path="tagName" cssClass="msg" />
		</div>
	</sf:form>
</div>
<script type="text/javascript">
	doDisabled();
</script>