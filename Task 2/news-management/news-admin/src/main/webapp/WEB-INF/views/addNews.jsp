<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<spring:url value="/saveNews" var="saveNewsUrl" htmlEscape="true" />

<div class="add-news-form">
	<sf:form class="newsList" action="${saveNewsUrl}" commandName="newsVO"
		onsubmit="return validateNewsVO();">
		<div align="center">
			<sf:select id="selectAuthor" path="author.authorId">
				<option value="" selected="selected" disabled="disabled"><spring:message
						code="locale.edit.add.news.authors" /></option>
				<c:forEach var="author" items="${authors}">
					<c:choose>
						<c:when test="${empty author.expired }">
							<option value="${author.authorId}"><c:out value="${author.authorName}"/></option>
						</c:when>
						<c:otherwise>
							<option value="${author.authorId}" disabled="disabled"><c:out value="${author.authorName}"/></option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</sf:select>
			<select id="multiSelect" multiple="multiple"  name="selectedTags">
				<c:forEach var="tag" items="${tags}">
					<option value="${tag.tagId}"><c:out value="${tag.tagName}"/></option>
				</c:forEach>
			</select>
			<sf:errors path="author.authorName" cssClass="msg" />
			<script type="text/javascript">
				$(document).ready(function() {
					$('#multiSelect').change(function() {
						console.log($(this).val());
					}).dropdownchecklist({
						emptyText : "...",
						width : 150
					})
				})
			</script>
			</div>
			<br>
		<table>
			<tbody>
				<tr>
					<td><spring:message code="locale.edit.add.news.title" /></td>
					<td><sf:input path="news.title" name="news-title"
							class="news-title" id="news-title" /></td>
				</tr>
				<tr><td colspan="2"><sf:errors path="news.title" cssClass="msg" /></td></tr>
				<tr>
					<td><spring:message code="locale.edit.add.news.date" /></td>
					<td><input type="text" name="news-creation-date" id="news-creation-date"
						value="<c:out value="${date}"/>" disabled="disabled"></td>
				</tr>
				<tr>
					<td><spring:message code="locale.edit.add.news.short-text" /></td>
					<td><sf:textarea path="news.shortText" name="news-short-text"
							class="news-short-text"></sf:textarea></td>
				</tr>
				<tr><td colspan="2"><sf:errors path="news.shortText" cssClass="msg" /></td></tr>
				<tr>
					<td><spring:message code="locale.edit.add.news.full-text" /></td>
					<td><sf:textarea path="news.fullText" name="news-full-text"
							class="news-full-text"></sf:textarea></td>
				</tr>
				<tr><td colspan="2"><sf:errors path="news.fullText" cssClass="msg" /></td></tr>
			</tbody>
		</table>
		
			<br>
			<br> <input type="submit" name="saveNews" id="button-save-news"
				value="<spring:message code="locale.edit.add.button.save"/>">
	</sf:form>
</div>