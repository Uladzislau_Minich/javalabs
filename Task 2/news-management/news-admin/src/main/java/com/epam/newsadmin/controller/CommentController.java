package com.epam.newsadmin.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsadmin.utils.CommentValidator;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.entity.NewsValueObject;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsValueObjectService;

/**
 * The class CommentController.
 * 
 * @author Uladzislau
 *
 */
@Controller
public class CommentController {
	@Autowired
	private NewsValueObjectService newsManagementService;
	@Autowired
	private CommentValidator validator;

	/**
	 * Save new comment
	 * 
	 * @param model
	 *            the model object
	 * @param comment
	 *            the comment object
	 * @param newsId
	 *            the news's id
	 * @param result
	 *            the binding result object
	 * @return the string of view's name
	 */
	@RequestMapping(value = "/addComment/{newsId}", method = RequestMethod.POST)
	public String addComment(Model model, @ModelAttribute("commentNews") Comment comment, @PathVariable Long newsId,
			BindingResult result) {
		validator.validate(comment, result);
		try {
			if (result.hasErrors()) {
				NewsValueObject newsVO = newsManagementService.findNews(newsId);
				model.addAttribute("newsVO", newsVO);
				model.addAttribute("commentNews", comment);
				model.addAttribute("currentMenu", "news-list-nav");
				return "news";
			}
			comment.setCreationDate(new Timestamp(new java.util.Date().getTime()));
			comment.setNewsId(newsId);
			Long commentId = newsManagementService.addComment(comment);
			comment.setCommentId(commentId);
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "redirect:/viewNews/" + newsId;
	}

	/**
	 * Delete comment
	 * 
	 * @param model
	 *            the model object
	 * @param commentId
	 *            the comment's id
	 * @param newsId
	 *            the news's id
	 * @return the string of view's name
	 */
	@RequestMapping(value = "/deleteComment/{commentId}&{newsId}", method = RequestMethod.GET)
	public String deleteComment(Model model, @PathVariable Long commentId, @PathVariable Long newsId) {
		try {
			newsManagementService.deleteComment(new Long[]{commentId});
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "redirect:/viewNews/" + newsId;
	}
}
