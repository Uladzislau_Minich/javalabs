package com.epam.newsadmin.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newscommon.entity.Comment;

/**
 * Class CommentValidator.
 * 
 * @author Uladzislau
 *
 */
@Component
public class CommentValidator implements Validator {
	/**
	 * Check that class matches Comment.class or is an child class of it
	 */
	@Override
	public boolean supports(Class<?> arg0) {
		return Comment.class.isAssignableFrom(arg0);
	}

	/**
	 * Validate comment text
	 */
	@Override
	public void validate(Object arg0, Errors arg1) {
		Comment commment = (Comment) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "text", "locale.error.add.comment.empty");
		if (commment.getText().length() > 300) {
			arg1.rejectValue("text", "locale.error.add.comment.overflow");
		}
	}

}
