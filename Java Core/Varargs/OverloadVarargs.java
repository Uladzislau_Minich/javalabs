package com.epam.javacore;

public class OverloadVarargs {

	public static void main(String[] args) {
		int varInt = 5;
		long varLong = 1L;
		double varDouble = 2.0;
		method(varInt);// call method with parameter int
		method(varInt);// call method with parameter int
		method(varLong);// call method with parameter long
		method(varInt);// call method with parameter int
		method(varLong);// call method with parameter long
		method(varDouble);// call method with parameter double
	}

	public static void method(int a) {
		System.out.println("Called with simple parameter int.");
	}

	public static void method(int... a) {
		System.out.println("Called with int varargs.");

	}

	public static void method(long a) {
		System.out.println("Called with simple parameter long.");
	}

	public static void method(long... a) {
		System.out.println("Called with long varargs.");
	}

	public static void method(double... a) {
		System.out.println("Called with double varargs.");
	}

	public static void method(double a) {
		System.out.println("Called with simple parameter double.");
	}

}
