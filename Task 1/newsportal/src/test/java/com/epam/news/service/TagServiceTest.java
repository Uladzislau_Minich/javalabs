package com.epam.news.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.implementation.TagServiceImpl;

/**
 * Class TagServiceTest. Contains methods for testing working a class
 * TagService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	@Mock
	private TagDAO tagDAOmock;
	@InjectMocks
	private TagServiceImpl tagServiceMock;

	private Tag tag;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		tagServiceMock = mock(TagServiceImpl.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(tagServiceMock.save(tag)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				tag = new Tag();
				tag.setTagId(123L);
				tag.setTagName("tagname");
				return tag.getTagId();
			}
		});
		Long idTag = tagServiceMock.save(tag);
		Long expectedId = new Long(123);
		Assert.assertEquals(expectedId, idTag);
	}

	@Test
	public void checkFindById() throws ServiceException {
		tag = null;
		when(tagServiceMock.findById(123L)).thenReturn(new Tag());
		tag = tagServiceMock.findById(123L);
		Assert.assertNotNull(tag);
	}

	@Test
	public void checkDeleteById() throws ServiceException {
		tag = new Tag();
		tag.setTagId(123L);
		when(tagServiceMock.deleteById(123L)).thenReturn(true);
		boolean fl = tagServiceMock.deleteById(123L);
		assertSame(true, fl);
	}

	@Test
	public void checkFindAllNewsTag() throws ServiceException {
		Long idNews = 1L;
		when(tagServiceMock.findAllNewsTag(idNews)).then(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> list = new ArrayList<Tag>();
				list.add(new Tag());
				list.add(new Tag());
				list.add(new Tag());
				return list;
			}
		});

		assertSame(3, tagServiceMock.findAllNewsTag(idNews).size());
	}

}
