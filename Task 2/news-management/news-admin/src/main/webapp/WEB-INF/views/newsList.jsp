<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<spring:url value="/deleteNews" var="deleteNewsUrl" htmlEscape="true" />
<spring:url value="/news/pages/1" var="newsListUrl" htmlEscape="true" />
<spring:url value="/resetFilter" var="resetFilterUrl" htmlEscape="true" />
<div align="center">
	<sf:form action="${newsListUrl}" commandName="searchCriteriaObject"
		method="GET">
		<sf:select id="authorSelect" name="authorSelect"
			path="authorId">
			<option selected="selected" disabled="disabled">...</option>
			<c:forEach var="author" items="${authors}">
				<c:choose>
					<c:when test="${author.authorId != searchCriteria.authorId}">
						<option value="${author.authorId}"><c:out
								value="${author.authorName}" /></option>
					</c:when>
					<c:otherwise>
						<option value="${author.authorId}" selected="selected"><c:out
								value="${author.authorName}" /></option>
					</c:otherwise>
				</c:choose>

			</c:forEach>
		</sf:select>
		<select id="multiSelect" multiple="multiple" name="tagsSelect">
			<c:forEach var="tag" items="${tags}">
				<c:forEach var="selectedTagId" items="${searchCriteria.tagIds}">
					<c:if test="${selectedTagId==tag.tagId}">
						<c:set var="flafForCheck" value="true" />
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${flafForCheck==true}">
						<option value="${tag.tagId}" selected="selected"><c:out
								value="${tag.tagName}" /></option>
					</c:when>
					<c:otherwise>
						<option value="${tag.tagId}"><c:out
								value="${tag.tagName}" /></option>
					</c:otherwise>
				</c:choose>
				<c:set var="flafForCheck" value="false" />
			</c:forEach>
		</select>
		<input type="submit"
			value="<spring:message code="locale.news-list.filter.button"/>" />
	</sf:form>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#multiSelect').change(function() {
				console.log($(this).val());
			}).dropdownchecklist({
				emptyText : "...",
				width : 150
			})
		})
	</script>
	<sf:form action="${resetFilterUrl }" method="GET">
		<input type="submit"
			value="<spring:message code="locale.news-list.reset-filter.button"/>" />
	</sf:form>
</div>
<br />
<sf:form action="${deleteNewsUrl}">
	<c:forEach var="newsVO" items="${listNews}">
		<div class="newsVO">
			<spring:url value="/viewNews/${newsVO.news.newsId}" var="viewNewsUrl"
				htmlEscape="true" />
			<div class="news-title">
				<a href="${viewNewsUrl}"><c:out value="${newsVO.news.title}" /></a>
			</div>
			<span class="news-author">(<spring:message
					code="locale.news-list.author" /> <c:out
					value="${newsVO.author.authorName}" />)
			</span> <span class="news-modification-date"><c:out
					value="${newsVO.news.modificationDate}" /></span> <br> <span
				class="news-short-text"><c:out
					value="${newsVO.news.shortText}" /></span> <br> <br>
			<div class="news-tags-list">
				<c:forEach var="tag" items="${newsVO.tags}">
					<span class="news-tags">#<c:out value="${tag.tagName}" />
					</span>
				</c:forEach>
			</div>
			<c:choose>
				<c:when test="${not empty newsVO.comments }">
					<div class="news-comments">
						<spring:message code="locale.news-list.comments" />
						(
						<c:out value="${fn:length(newsVO.comments)}" />
						)
					</div>
				</c:when>
				<c:otherwise>
					<div class="news-comments">
						<spring:message code="locale.news-list.comments" />
						(0)
					</div>
				</c:otherwise>
			</c:choose>
			<div class="edit-news-button">
				<spring:url value="/editNews/${newsVO.news.newsId}"
					var="editNewsUrl" htmlEscape="true" />
				<a href="${editNewsUrl}"><spring:message
						code="locale.news-list.edit" /></a>
			</div>
			<input type="checkbox" name="selectedNews"
				value="${newsVO.news.newsId}"> <br> <br>
		</div>
	</c:forEach>
	<c:choose>
		<c:when test="${not empty listNews }">
			<input type="submit" name="deleteNews" id="button-save-news"
				value="<spring:message code="locale.news-list.delete"/>"
				style="float: left;">
		</c:when>
		<c:otherwise>
			<div align="center">
				<spring:message code="locale.news-list.empty.search-criteria" />
			</div>
		</c:otherwise>
	</c:choose>
</sf:form>
<br>
<br>
<div align="center">
	<c:forEach var="page" step="1" begin="1" end="${countPage}">
		<spring:url value="/news/pages/${page}" var="newsListUrl"
			htmlEscape="true" />
		<c:choose>
			<c:when test="${page==currentPage}">
				<a href="${newsListUrl}" id="currentPage" class="page">${page}</a>
			</c:when>
			<c:otherwise>
				<a href="${newsListUrl}" class="page">${page}</a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</div>

