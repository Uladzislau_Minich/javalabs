package com.epam.newsclient.utils;

import com.epam.newscommon.entity.Comment;

public class CommentValidator {
	public static String validate(Comment comment){
		if(comment.getText().trim().length()==0){
			return "news.add.comment.error.empty";
		}else if(comment.getText().trim().length()>300){
			return "news.add.comment.error.overflow";
		}
		return null;
	}
}
