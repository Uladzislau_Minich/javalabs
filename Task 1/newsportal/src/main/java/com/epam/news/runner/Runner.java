package com.epam.news.runner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.entity.SearchCriteriaObject;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsValueObjectService;

@PropertySource("classpath:log4j.properties")
public class Runner {
	private static Logger log = Logger.getLogger(Runner.class);

	static {
		PropertyConfigurator.configure("resources/log4j.properties");
		BasicConfigurator.configure();
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
		NewsValueObjectService service = (NewsValueObjectService) context.getBean("NewsValueObjectServiceImpl");
		try {
			/* Show all news */
			List<NewsValueObject> list = service.findAllNews();
			for(NewsValueObject ob:list){
				log.info(ob);
			}
			log.info("\n\n");
			
			/*count news */
			int amount = service.countNews();
			log.info("Amount of all news: "+amount+"\n\n");
			
			/* find news by search criteria */
			SearchCriteriaObject sc = new SearchCriteriaObject();
			Author author = new Author();
			author.setAuthorId((long) 5);
			Tag tag = new Tag();
			tag.setTagId((long) 7);
			List<Tag> tags = new ArrayList<Tag>();
			tags.add(tag);
			sc.setAuthor(author);
			sc.setTags(tags);
			list = service.findBySearchCriteria(sc);
			for(NewsValueObject ob:list){
				log.info(ob);
			}
			log.info("\n\n");
			
			/* add news*/
			News news = new News();
			news.setFullText("Full text");
			news.setModificationDate(new Date());
			news.setCreationTime(new Timestamp(new java.util.Date().getTime()));
			news.setShortText("Short text");
			news.setTitle("Title");
			author = new Author();
			author.setAuthorName("Author name");
			author.setExpired(null);
			service.saveNews(news, author, null);
			
			/* add author */
//			author.setAuthorName("NEW AUTHOR");
//			author.setExpired(null);
//			service.saveAuthor(author);
			
			
			
		} catch (ServiceException e) {
			log.error("Error", e);
		}
		
	}
}
