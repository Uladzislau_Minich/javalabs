package com.epam.news.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.implementation.NewsDAOImpl;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.implementation.NewsServiceImpl;

/**
 * Class NewsServiceTest. Contains methods for testing working a class
 * NewsService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
	@Mock
	private NewsDAOImpl newsDAOMock;
	@InjectMocks
	private NewsServiceImpl newsServiceMock;
	private News news;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		newsDAOMock = mock(NewsDAOImpl.class);
		newsServiceMock = mock(NewsServiceImpl.class);
		news = mock(News.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(newsServiceMock.save(news)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				news = new News();
				news.setNewsId(123L);
				news.setTitle("Title");
				news.setFullText("Full text");
				news.setShortText("Short Text");
				return news.getNewsId();
			}
		});
		assertEquals(new Long(123), newsServiceMock.save(news));
	}

	@Test
	public void checkFindById() throws ServiceException {
		when(newsServiceMock.findById(any(Long.class))).then(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = new News();
				news.setNewsId(123L);
				news.setTitle("Title");
				news.setFullText("Full text");
				news.setShortText("Short Text");
				return news;
			}
		});
		news = new News();
		news.setNewsId(123L);
		news.setTitle("Title");
		news.setFullText("Full text");
		news.setShortText("Short Text");
		assertEquals(news, newsServiceMock.findById(123L));
	}

	@Test
	public void checkUpdate() throws ServiceException, DAOException {
		news = new News();
		newsDAOMock.update(news);
		newsDAOMock.update(news);
		verify(newsDAOMock, times(2)).update(news);
	}

	@Test
	public void checkDeleteById() throws ServiceException, DAOException {
		newsDAOMock.deleteById(123L);
		verify(newsDAOMock).deleteById(123L);
	}

	@Test
	public void checkFindAll() throws ServiceException {
		when(newsServiceMock.findAllNews()).then(new Answer<List<News>>() {

			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> list = new ArrayList<>();
				list.add(new News());
				list.add(new News());
				list.add(new News());
				return list;
			}
		});
		List<News> list = newsServiceMock.findAllNews();
		assertSame(3, list.size());
	}

	@Test
	public void checkSaveNewsAuthor() throws ServiceException {
		when(newsServiceMock.saveNewsAuthors(any(Long.class), any(Long.class))).thenReturn(true);
		assertSame(true, newsServiceMock.saveNewsAuthors(1L, 2L));
	}

	@Test
	public void checkDeleteNewsAuthor() throws ServiceException {
		when(newsServiceMock.deleteNewsAuthor(any(Long.class))).thenReturn(true);
		assertSame(true, newsServiceMock.deleteNewsAuthor(2L));
	}
}
