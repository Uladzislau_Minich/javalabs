package com.epam.newscommon.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Locale;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newscommon.dao.AuthorDAO;
import com.epam.newscommon.dao.impl.AuthorDAOImpl;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * Class AuthorDAOTest.  Contains methods for testing working a class AuthorDAO.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-context-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class AuthorDAOTest {

	@Autowired
	private AuthorDAO authorDAO;

	/**
	 * Load data to database.
	 * 
	 * @throws FileNotFoundException
	 * @throws DatabaseUnitException
	 * @throws SQLException
	 */
	@Before
	public void doSetup() throws FileNotFoundException, DatabaseUnitException, SQLException {
		Locale.setDefault(Locale.ENGLISH);
		Connection con = ((AuthorDAOImpl) authorDAO).getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-author-content.xml")) };

		DatabaseOperation.CLEAN_INSERT.execute(
				new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new CompositeDataSet(datasets));
		con.close();
	}

	/**
	 * Clean database after testing
	 * 
	 * @throws DataSetException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 */
	@After
	public void cleanDB() throws DataSetException, FileNotFoundException, SQLException, DatabaseUnitException {
		Connection con = ((AuthorDAOImpl) authorDAO).getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		DatabaseOperation.DELETE_ALL.execute(new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete-content.xml")));
		con.close();
	}

	@Test
	public void checkCreateAndDelete() throws DAOException {
		Author author = new Author();
		author.setAuthorName("Author name");
		author.setExpired(null);
		Long unexpectedAuthorId = -1L;
		Long actualAuthorId = authorDAO.create(author);
		Assert.assertNotEquals(unexpectedAuthorId, actualAuthorId);
		author = authorDAO.findById(actualAuthorId);
		Assert.assertNotNull(author);
		authorDAO.deleteById(new Long[]{actualAuthorId});
		assertSame(3, authorDAO.findAllAuthors());
	}

	@Test
	public void checkUpdate() throws DAOException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Author name");
		author.setExpired(null);
		authorDAO.update(author);
		author = authorDAO.findById(1L);
		Assert.assertEquals("Author name", author.getAuthorName());
	}

	@Test
	public void checkFindAuthorsByNews() throws DAOException {
		Author author = authorDAO.findAuthorByNews(1L);
		Assert.assertNotNull(author);
	}
	
	@Test(expected = DAOException.class)
	public void checkFailCreateAuthor() throws DAOException{
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName(null);
		author.setExpired(null);
		authorDAO.create(author);
	}
	
}
