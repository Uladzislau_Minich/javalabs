package com.epam.newsclient.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class PaganitionTag extends TagSupport {
	private int countNews;

	@Override
	public int doStartTag() throws JspException {
		int i = 0;
		try {
			for (i = 1; i <= countNews / 5; i++) {
				 pageContext.getOut().write(
				 "<a href=\"controller?command=show_list_news&indexNewsPage="
				 +i+ "\">" +i+" </a>");
//				pageContext.getOut()
//						.write("<form action=\"Controller\" method=\"post\">"
//								+ "<input type=\"submit\" value=\""+i+"\"> <input"
//								+ "type=\"hidden\" name=\"command\" value=\"show_list_news\" /> <input"
//								+ "type=\"hidden\" name=\"indexNewsPage\" value=\""+i+"\" /></form>");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	public void setCountNews(int countNews) {
		this.countNews = countNews;
	}
}
