package com.epam.newsadmin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsadmin.utils.AuthorValidator;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.NewsValueObjectService;

/**
 * The class AuthorController.
 * 
 * @author Uladzislau
 *
 */
@Controller
public class AuthorController {
	
	@Autowired
	private NewsValueObjectService newsManagementService;
	@Autowired
	private AuthorValidator validator;

	/**
	 * Forward to add/edit author page
	 * 
	 * @param model
	 *            the model object
	 * @return the string of view name
	 */
	@RequestMapping(value = "/addAuthor", method = RequestMethod.GET)
	public String toAddAuthor(Model model) {
		List<Author> authors = null;
		try {
			authors = newsManagementService.findAllAuthors();
			model.addAttribute("authors", authors);
			model.addAttribute("currentMenu", "add-author-nav");
			model.addAttribute("authorEdit", new Author());
			model.addAttribute("newAuthor", new Author());
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "authors";
	}

	/**
	 * Update author's data
	 * 
	 * @param model
	 *            the model object
	 * @param author
	 *            the author object
	 * @param result
	 *            the binding result object
	 * @return the string of view name
	 */
	@RequestMapping(value = "/news-admin/saveEditAuthor", method = RequestMethod.POST)
	public String saveEditAuthor(Model model, @ModelAttribute("authorEdit") Author author, BindingResult result) {
		System.out.println(author);
		validator.validate(author, result);
		try {
			if (result.hasErrors()) {
				List<Author> authors = newsManagementService.findAllAuthors();
				model.addAttribute("authors", authors);
				model.addAttribute("currentMenu", "add-author-nav");
				model.addAttribute("authorEdit", author);
				model.addAttribute("newAuthor", new Author());
				return "authors";
			}
			newsManagementService.updateAuthor(author);
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/addAuthor";
	}

	/**
	 * Expired author
	 * 
	 * @param model
	 *            the model object
	 * @param authorId
	 *            the author id
	 * @return the string of view name
	 */
	@RequestMapping(value = "/expiredAuthor/{authorId}", method = RequestMethod.GET)
	public String expiredAuthor(Model model, @PathVariable("authorId") Long authorId) {

		try {
			Author author = newsManagementService.findAuthorById(authorId);
			newsManagementService.expiredAuthor(author);
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/addAuthor";
	}

	/**
	 * Save new author
	 * 
	 * @param model
	 *            the model object
	 * @param author
	 *            the author object
	 * @param result
	 *            the binding result object
	 * @return the string of view name
	 */
	@RequestMapping(value = "/saveNewAuthor", method = RequestMethod.POST)
	public String saveAuthors(Model model, @ModelAttribute("newAuthor") Author author, BindingResult result) {
		validator.validate(author, result);
		try {
			if (result.hasErrors()) {
				List<Author> authors = newsManagementService.findAllAuthors();
				model.addAttribute("authors", authors);
				model.addAttribute("currentMenu", "add-author-nav");
				model.addAttribute("authorEdit", new Author());
				model.addAttribute("newAuthor", author);
				return "authors";
			}
			newsManagementService.addAuthor(author);
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/addAuthor";
	}
}
