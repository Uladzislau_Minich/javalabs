package com.epam.newscommon.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.newscommon.dao.impl.CommentDAOImpl;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.service.impl.CommentServiceImpl;

/**
 * Class CommentServiceTest. Contains methods for testing working a class
 * CommentService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	@Mock
	private CommentDAOImpl commentDAOMock;
	@InjectMocks
	private CommentServiceImpl commentServiceMock;

	/**
	 * Initialize objects
	 */
	@Before
	public void setup() {
		commentDAOMock = mock(CommentDAOImpl.class);
		commentServiceMock = mock(CommentServiceImpl.class);
	}

	@Test
	public void checkCallMethod() throws DAOException, ServiceException {
		Comment comment = new Comment();
		commentDAOMock.create(comment);
		commentDAOMock.deleteById(new Long[]{1L});
		commentDAOMock.findById(1L);
		commentDAOMock.update(comment);
		commentDAOMock.findAllNewsComment(1L);
		commentDAOMock.findAllNewsComment(1L);
		verify(commentDAOMock).create(comment);
		verify(commentDAOMock, times(2)).findAllNewsComment(1L);
		verify(commentDAOMock).deleteById(new Long[]{1L});
		verify(commentDAOMock).findById(1L);
		verify(commentDAOMock).update(comment);

	}

	@Test
	public void checkFindById() throws ServiceException {
		Comment comment = new Comment();
		Long id = 123L;
		comment.setCommentId(id);
		when(commentServiceMock.findById(123L)).thenReturn(comment);
		assertSame(id, commentServiceMock.findById(123L).getCommentId());
	}

	@Test
	public void checkSave() throws ServiceException {
		Comment comment = new Comment();
		comment.setCommentId(123L);
		when(commentServiceMock.save(comment)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				Comment com = new Comment();
				com.setCommentId(123L);
				com.setText("Comment text");
				com.setCreationDate(new Timestamp(new java.util.Date().getTime()));
				return com.getCommentId();
			}

		});
		assertSame(comment.getCommentId(), commentServiceMock.save(comment));
	}

	@Test(expected = ServiceException.class)
	public void checkCreate() throws ServiceException {
		when(commentServiceMock.save(new Comment())).thenThrow(new ServiceException());
		assertSame(true, commentServiceMock.save(new Comment()));
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void checkShowAllNewsComment() throws ServiceException {
		Long idNews = 1L;
		when(commentServiceMock.findAllNewsComment(idNews)).then(new Answer() {
			@Override
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> list = new ArrayList<Comment>();
				list.add(new Comment());
				list.add(new Comment());
				list.add(new Comment());
				return list;
			}
		});
		assertSame(3, commentServiceMock.findAllNewsComment(idNews).size());
	}
}
