package com.epam.newsclient.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newscommon.exception.ServiceException;

/**
 * Interface ICommand.
 * 
 * @author Uladzislau
 *
 */
public interface ICommand {
	/**
	 * Implementation contains list of instructions which do each command
	 * 
	 * @param request
	 *            the http servlet request object
	 * @return the string of page's name
	 * @throws ServiceException
	 */
	String execute(HttpServletRequest request) throws ServiceException;
}