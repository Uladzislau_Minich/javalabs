<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.2.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.11.2.custom.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/ui.dropdownchecklist-1.5-min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/news-admin.js" />"></script>
    <!--  <script type="text/javascript" src="<c:url value="/resources/js/validation.js" />"></script>-->
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/news-admin-style.css" />">
	
</head>
<body>
	<div class="header">
		<tiles:insertAttribute name="header" />
	</div>
	<div class="navigation">
		<tiles:insertAttribute name="navigation" />
	</div>
	<div class="content">
		<tiles:insertAttribute name="body" />
	</div>
	<div class="footer" align="center">
		<tiles:insertAttribute name="footer"/>
	</div>
</body>

</html>