package com.epam.news.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.implementation.AuthorDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.implementation.AuthorServiceImpl;

/**
 * Class AuthorServiceTest. Contains methods for testing working a class
 * AuthorService.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	@Mock
	private AuthorDAOImpl authorDAOMock;
	@InjectMocks
	private AuthorServiceImpl authorServiceMock;
	private Author author;

	/**
	 * Initialize objects
	 */
	@Before
	public void init() {
		author = mock(Author.class);
		authorDAOMock = mock(AuthorDAOImpl.class);
	}

	@Test
	public void checkSave() throws ServiceException {
		when(authorServiceMock.save(author)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				author = new Author();
				author.setAuthorId(123L);
				author.setAuthorName("Author name");
				return author.getAuthorId();
			}
		});
		Long idAuthor = new Long(authorServiceMock.save(author));
		Long expectedId= new Long(123);
		Assert.assertEquals(expectedId, idAuthor);
	}

	@Test
	public void checkFindByName() throws ServiceException {
		when(authorServiceMock.findAuthorByName(any(String.class))).then(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				author = new Author();
				author.setAuthorId(123L);
				author.setAuthorName("Author name");
				return author;
			}
		});
		assertNotNull(authorServiceMock.findAuthorByName("Author name"));
	}

	@Test
	public void checkFindById() throws ServiceException {
		Long id = 123L;
		when(authorServiceMock.findById(id)).then(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				author = new Author();
				author.setAuthorId(123L);
				author.setAuthorName("Author name");
				return author;
			}
		});
		author = authorServiceMock.findById(id);
		assertSame("Author name", author.getAuthorName());
	}

	@Test
	public void checkUpdate() throws ServiceException, DAOException {
		Author author = new Author();
		authorDAOMock.update(author);
		verify(authorDAOMock).update(author);
	}

	@Test
	public void checkDelete() throws ServiceException, DAOException {
		authorDAOMock.deleteById(12L);
		verify(authorDAOMock).deleteById(12L);
	}

	@Test
	public void checkExpiredAuthor() throws ServiceException {
		when(authorServiceMock.expiredAuthor(any(Long.class))).thenReturn(true);
		assertSame(true, authorServiceMock.expiredAuthor(1L));
	}

	@Test
	public void checkFindNotExpiredAuthors() throws ServiceException {
		when(authorServiceMock.findNotExpiredAuthors()).then(new Answer<List<Author>>() {

			@Override
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> list = new ArrayList<>();
				list.add(new Author());
				list.add(new Author());
				return list;
			}
		});
		List<Author> list = authorServiceMock.findNotExpiredAuthors();
		assertSame(list.size(), 2);
	}
}
