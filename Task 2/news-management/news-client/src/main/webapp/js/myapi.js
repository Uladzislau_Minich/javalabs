function validationComment() {
	var a = $('#textareaNewComment').val();
	var flag = true;
	$('.msg').empty();
	$('#textareaNewComment').removeClass('error');
	if (a.trim() == '') {
		$('#textareaNewComment').addClass('error');
		$('#addComment')
				.before(
						'<div class="msg"> <fmt:message key="news.add.comment.error.empty" /></div>');
		flag = false;
	}
	if (a.trim().length > 300) {
		$('#textareaNewComment').addClass('error');
		$('#addComment')
				.before(
						'<div class="msg"><fmt:message key="news.add.comment.error.overflow" /></div>');
		flag = false;
	}
	return flag;
}


