package com.epam.news.dao;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.news.dao.implementation.NewsDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteriaObject;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
/**
 * Class NewsDAOTest.  Contains methods for testing working a class NewsDAO.
 * 
 * @author Uladzislau_Minich
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-context-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOTest {

	@Autowired
	private NewsDAO newsDAO;

	/**
	 * Load data to database.
	 * 
	 * @throws FileNotFoundException
	 * @throws DatabaseUnitException
	 * @throws SQLException
	 */
	@Before
	public void doSetup() throws FileNotFoundException, DatabaseUnitException, SQLException {
		Locale.setDefault(Locale.ENGLISH);
		Connection con = NewsDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-tag-content.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-author-content.xml")) };

		DatabaseOperation.CLEAN_INSERT.execute(
				new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new CompositeDataSet(datasets));
		con.close();
	}

	/**
	 * Clean database after testing
	 * 
	 * @throws DataSetException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 */
	@After
	public void cleanDB() throws DataSetException, FileNotFoundException, SQLException, DatabaseUnitException {
		Connection con = NewsDAOImpl.getDataSource().getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		DatabaseOperation.DELETE_ALL.execute(new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase()),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete-content.xml")));
		con.close();
	}

	@Test
	public void checkCreateAndDelete() throws DAOException {
		News news = new News();
		news.setFullText("Full text");
		news.setModificationDate(new Date());
		news.setCreationTime(new Timestamp(new java.util.Date().getTime()));
		news.setShortText("Short text");
		news.setTitle("Title");
		Long actualNewsId = newsDAO.create(news);
		Long unexpectedId = new Long(-1);
		Assert.assertNotEquals(unexpectedId, actualNewsId);
		assertSame(true, newsDAO.deleteById(actualNewsId));
	}

	@Test
	public void checkUpdate() throws DAOException {
		News news = new News();
		news.setFullText("Full text");
		news.setModificationDate(new Date());
		news.setCreationTime(new Timestamp(new java.util.Date().getTime()));
		news.setShortText("Short text");
		news.setTitle("Title");
		news.setNewsId(1L);
		assertSame(true, newsDAO.update(news));
	}

	@Test()
	public void checkFindById() throws DAOException {
		Assert.assertNotNull(newsDAO.findById(1L));
	}

	@Test
	public void checkFindAllNews() throws DAOException {
		List<News> list = newsDAO.findAllNews();
		assertSame(3, list.size());
	}

	@Test
	public void checkSaveAndDeleteNewsAuthor() throws DAOException {
		Assert.assertEquals(true, newsDAO.saveNewsAuthors(1L, 1L));
		Assert.assertEquals(true, newsDAO.deleteNewsAuthor(1L));
	}

	@Test
	public void checkSaveAndDeleteNewsTag() throws DAOException {
		Assert.assertEquals(true, newsDAO.addNewsTag(1L, 1L));
		Assert.assertEquals(true, newsDAO.deleteNewsTag(1L));
	}

	@Test
	public void checkFindBySeachCriteria() throws DAOException {
		SearchCriteriaObject sc = new SearchCriteriaObject();
		List<Tag> tags = new ArrayList<>();
		Tag tag = new Tag();
		tag.setTagId(1L);
		Tag tag2 = new Tag();
		tag2.setTagId(2L);
		tags.add(tag);
		tags.add(tag2);
		Author author = new Author();
		author.setAuthorId(1L);
		sc.setAuthor(author);
		sc.setTags(tags);
		List<News> list = newsDAO.findNewsBySearchCriteria(sc);
		Assert.assertEquals(1, list.size());
	}

	@Test
	public void checkFindAllAuthorNews() throws DAOException {
		List<News> list = newsDAO.findAllAuthorsNews(1L);
		Assert.assertEquals(1, list.size());
	}

}
