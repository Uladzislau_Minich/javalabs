package com.epam.newsadmin.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newscommon.entity.Tag;
/**
 * Class TagValidator.
 * 
 * @author Uladzislau
 *
 */
@Component
public class TagValidator implements Validator {
	/**
	 * Check that class matches Tag.class or is an child class of it
	 */
	@Override
	public boolean supports(Class<?> arg0) {
		return Tag.class.isAssignableFrom(arg0);
	}
	/**
	 * Validate tag name
	 */
	@Override
	public void validate(Object arg0, Errors arg1) {
		Tag tag = (Tag) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "tagName", "locale.error.add.edit.tag.empty");
		if (tag.getTagName().length() > 30) {
			arg1.rejectValue("tagName", "locale.error.add.edit.tag.overflow");
		}
	}

}
