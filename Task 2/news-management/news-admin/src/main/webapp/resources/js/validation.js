/**
 * Contain function, which validate data of object
 *   (while create new objects or edit existing object 
 */

/**
 * validate news value object
 * 
 */
function validateNewsVO() {
	var flag = true;
	if (!validateNewsTitle()) {
		flag = false;
	}
	if (!validateNewsShortText()) {
		flag = false;
	}
	if (!validateNewsFullText()) {
		flag = false;
	}
	if (!validateNewsDate()) {
		flag = false;
	}
	if (!validateNewsAuthor()) {
		flag = false;
	}
	return flag;
}

/** 
 * validate news title
 */
function validateNewsTitle() {
	var flag = true;
	var value = $('.news-title').val();
	$('.msg').empty();
	$('.news-title').removeClass('error');
	if (value.trim() == '') {
		$('.news-title').addClass('error');
		$('.news-title')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.edit.new.title.empty"/></div>');
		flag = false;
	}
	if (value.trim().length > 30) {
		$('.news-title').addClass('error');
		$('.news-title')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.edit.new.title.overflow"/></div>');
		flag = false;
	}
	return flag;
}
/**
 * validate news short text
 */
function validateNewsShortText() {
	var flag = true;
	var value = $('.news-short-text').val();
	$('.news-short-text').removeClass('error');
	if (value.trim() == '') {
		$('.news-short-text').addClass('error');
		$('.news-short-text')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.edit.new.short-text.empty"/></div>');
		flag = false;
	}
	if (value.trim().length > 300) {
		$('.news-short-text').addClass('error');
		$('.news-short-text')
				.after(
						'<div class="msg"><spring:message code="locale.error.add.edit.new.short-text.overflow"/></div>');
		flag = false;
	}
	return flag;
}

/**
 * validate news full text
 */
function validateNewsFullText() {
	var flag = true;
	var value = $('.news-full-text').val();
	$('.news-full-text').removeClass('error');
	if (value.trim() == '') {
		$('.news-full-text').addClass('error');
		$('.news-full-text')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.edit.new.full-text.empty"/></div>');
		flag = false;
	}
	if (value.trim().length > 3000) {
		$('.news-full-text').addClass('error');
		$('.news-full-text')
				.after(
						'<div class="msg"><spring:message code="locale.error.add.edit.new.full-text.overflow"/></div>');
		flag = false;
	}
	return flag;
}

/**
 * validate news creation(adding new news)/modification(editing news)
 */
function validateNewsDate() {
	var flag = true;
	var value = $('#news-creation-date').val();
	$('#news-creation-date').removeClass('error');
	if (value.trim() == '') {
		$('#news-creation-date').addClass('error');
		$('#news-creation-date')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.edit.new.date.empty"/></div>');
		flag = false;
	}
	if (!(/[0-9]{4}\/[012]{2}\/[012]{1}[0-9]{1}/.test(value.trim()))
			|| value.trim() > 10) {
		$('#news-creation-date').addClass('error');
		$('#news-creation-date')
				.after(
						'<div class="msg"><spring:message code="locale.error.add.edit.new.date.overflow"/></div>');
		flag = false;
	}
	return flag;
}

/**
 * validate news author
 */ 
function validateNewsAuthor() {
	var flag = true;
	var value = $('#selectAuthor').val();
	$('#selectAuthor').removeClass('error');
	if (value == null) {
		$('#selectAuthor').addClass('error');
		$('#selectAuthor')
				.after(
						'<div class="msg" style="position:absolute;margin-top:25px;margin-left:-130px;"> <spring:message code="locale.error.add.edit.author.empty"/></div>');
		flag = false;
	}
	return flag;
}

/**
 *  validate comment text
 */
function validationComment() {
	var a = $('#new-comment-text').val();
	var flag = true;
	$('.msg').empty();
	$('#new-comment-text').removeClass('error');
	if (a.trim() == '') {
		$('#new-comment-text').addClass('error');
		$('#new-comment-button')
				.after(
						'<div class="msg"> <spring:message code="locale.error.add.comment.empty"/></div>');
		flag = false;
	}
	if (a.trim().length > 300) {
		$('#new-comment-text').addClass('error');
		$('#new-comment-button')
				.after(
						'<div class="msg"><spring:message code="locale.error.add.comment.overflow"/></div>');
		flag = false;
	}
	return flag;
}

/**
 * validate tag name
 */
function validateTagName(id) {
	var flag = true;
	var value = $('#' + id).val();
	$('.msg').empty();
	$('#' + id).removeClass('error');
	if (value.trim() == '') {
		$('#' + id).addClass('error');
		$('#' + id)
				.after(
						'<div class="msg" style="position:absolute; margin-top:35px; margin-left:-50%;"> <spring:message code="locale.error.add.edit.new.full-text.empty"/></div>');
		flag = false;
	}
	if (value.trim().length > 20) {
		$('#' + id).addClass('error');
		$('#' + id)
				.after(
						'<div class="msg" style="margin-top:25px;margin-left:-100px;"><spring:message code="locale.error.add.edit.new.full-text.overflow"/></div>');
		flag = false;
	}
	return flag;
}

/**
 * validate author name
 */
function validateAuthorName(id) {
	var flag = true;
	var value = $('#' + id).val();
	$('.msg').empty();
	$('#' + id).removeClass('error');
	if (value.trim() == '') {
		$('#' + id).addClass('error');
		$('#' + id)
				.after(
						'<div class="msg" style="position:absolute; margin-top:35px; margin-left:-50%;"> <spring:message code="locale.error.add.edit.new.full-text.empty"/></div>');
		flag = false;
	}
	if (value.trim().length > 20) {
		$('#' + id).addClass('error');
		$('#' + id)
				.after(
						'<div class="msg" style="margin-top:25px;margin-left:-100px;"><spring:message code="locale.error.add.edit.new.full-text.overflow"/></div>');
		flag = false;
	}
	return flag;
}
