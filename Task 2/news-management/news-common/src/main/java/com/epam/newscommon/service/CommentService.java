package com.epam.newscommon.service;

import java.util.List;

import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.entity.Comment;

/**
 * Interface CommentService. Classes which implements this interface contain
 * action with CommentDAO in DAO layer
 * 
 * @author Uladzislau_Minich
 *
 */
public interface CommentService extends BaseService<Comment> {
	/**
	 * Find all news's comments
	 * 
	 * @return list of objects Comment
	 * @throws ServiceException
	 */
	List<Comment> findAllNewsComment(Long newsId) throws ServiceException;

	/**
	 * Delete comments by news ids
	 * 
	 * @param newsIds
	 *            the list of news's ids
	 * @throws ServiceException
	 */
	void deleteCommentsByNewsId(Long[] newsIds) throws ServiceException;
}
