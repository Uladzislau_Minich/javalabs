import java.util.ArrayList;

public class StaticBlock {
	
	//processing exception - ok
	static {
		
		try{
			Integer.parseInt("123s");
		}catch(NumberFormatException e){
			System.out.println("error");
		}
		s = "str";
	}
	//ExceptionInInitializerError
	//
	//static {
	//	Integer.parseInt("123s");
	//	System.out.println("error");
	//	
	//}

	//creating new object - ok
	static{
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		for(Integer i:list){
			//System.out - ok
			System.out.println(i);
		}
		listN=list;
	}

	//Initializer does not complete normally 
	//static{
	//	return;
	//}

	static String s;
	static ArrayList<Integer> listN;

	public static void main(String[] args) {
		System.out.println(s);
		System.out.println(listN);
	}
}
