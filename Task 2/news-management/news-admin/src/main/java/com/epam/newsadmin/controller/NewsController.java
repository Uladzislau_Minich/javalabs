package com.epam.newsadmin.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newscommon.service.NewsValueObjectService;
import com.epam.newscommon.utils.SearchCriteriaObject;
import com.epam.newscommon.entity.NewsValueObject;
import com.epam.newscommon.entity.Tag;
import com.epam.newsadmin.utils.NewsValidator;
import com.epam.newscommon.entity.Author;
import com.epam.newscommon.entity.Comment;
import com.epam.newscommon.exception.ServiceException;

/**
 * Class NewsController.
 * 
 * @author Uladzislau
 *
 */
@Controller
public class NewsController {
	static {
		Locale.setDefault(Locale.ENGLISH);
	}

	@Autowired
	private NewsValueObjectService newsManagementService;
	@Autowired
	private NewsValidator validator;

	/**
	 * Redirect to first news page.
	 * 
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = { "/", "/news" }, method = RequestMethod.GET)
	public String viewAllNews() {
		return "redirect:/news/pages/1";
	}

	/**
	 * Show list news on page
	 * 
	 * @param model
	 *            the model object
	 * @param page
	 *            the number of page
	 * @param session
	 *            the session object
	 * @param seacrCriteria
	 *            the search criteria object
	 * @param tagIds
	 *            the list of tag's ids
	 * @return the string of view's name
	 */
	@RequestMapping(value = { "/news/pages/{page}", "/news-admin/news/pages/{page}" }, method = RequestMethod.GET)
	public String viewNewsPerPage(Model model, @PathVariable int page, HttpSession session,
			@ModelAttribute("searchCriteriaObject") SearchCriteriaObject seacrCriteria,
			@RequestParam(value = "tagsSelect", required = false) List<Long> tagIds) {
		int countNewsPerPage = 3;
		int countNews = 0;
		int startIndex = (page - 1) * countNewsPerPage + 1;
		int endIndex = page * countNewsPerPage;
		List<Author> authors = null;
		List<Tag> tags = null;
		List<NewsValueObject> newsList = null;
		SearchCriteriaObject sc = (SearchCriteriaObject) session.getAttribute("searchCriteria");
		try {
			if (seacrCriteria.getAuthorId() != null || tagIds != null) {
				sc = createSearchCriteria(seacrCriteria.getAuthorId(), tagIds);
			}
			newsList = newsManagementService.findNewsPerPage(sc, startIndex, endIndex);
			countNews = newsManagementService.countNews(sc);
			session.setAttribute("searchCriteria", sc);
			int countPage = 1;
			if (countNews % countNewsPerPage != 0) {
				countPage = (countNews / countNewsPerPage) + 1;
			} else {
				countPage = (countNews / countNewsPerPage);
			}
			authors = newsManagementService.findAllAuthors();
			tags = newsManagementService.findAllTags();
			model.addAttribute("authors", authors);
			model.addAttribute("tags", tags);
			model.addAttribute("listNews", newsList);
			model.addAttribute("countPage", countPage);
			model.addAttribute("currentPage", page);
			model.addAttribute("searchCriteriaObject", new SearchCriteriaObject());
			model.addAttribute("currentMenu", "news-list-nav");
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "newsList";
	}

	/**
	 * Show single news
	 * 
	 * @param model
	 *            the model object
	 * @param newsId
	 *            the news's id
	 * @return the string of view's name
	 */
	@RequestMapping(value = { "/viewNews/{newsId}", "/news-admin/viewNews/{newsId}" }, method = RequestMethod.GET)
	public String viewNews(Model model, @PathVariable long newsId) {
		NewsValueObject newsVO = null;
		try {
			newsVO = newsManagementService.findNews(newsId);
			model.addAttribute("newsVO", newsVO);
			model.addAttribute("commentNews", new Comment());
			model.addAttribute("currentMenu", "news-list-nav");
		} catch (ServiceException e) {
			return "errorPage";
		}
		if (newsVO == null) {
			return "errorPage";
		}
		return "news";
	}

	/**
	 * Redirect to page for adding news
	 * 
	 * @param model
	 *            the model object
	 * @return the string of view's name
	 */
	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public String toAddNews(Model model) {
		List<Author> authors = null;
		List<Tag> tags = null;
		try {
			authors = newsManagementService.findAllAuthors();
			tags = newsManagementService.findAllTags();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			model.addAttribute("date", dateFormat.format(date));
			model.addAttribute("authors", authors);
			model.addAttribute("tags", tags);
			model.addAttribute("newsVO", new NewsValueObject());
			model.addAttribute("currentMenu", "add-news-nav");
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "addNews";
	}

	/**
	 * Redirect to page for editing news
	 * 
	 * @param model
	 *            the model object
	 * @param newsId
	 *            the news's id
	 * @return the string of view's name
	 */
	@RequestMapping(value = { "/news-admin/editNews/{newsId}", "/editNews/{newsId}" }, method = RequestMethod.GET)
	public String toEditNews(Model model, @PathVariable Long newsId) {
		List<Author> authors = null;
		List<Tag> tags = null;
		NewsValueObject news = null;
		boolean flag = false;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			authors = newsManagementService.findAllAuthors();
			tags = newsManagementService.findAllTags();
			news = newsManagementService.findNews(newsId);
			model.addAttribute("date", dateFormat.format(news.getNews().getModificationDate()));
			model.addAttribute("authors", authors);
			model.addAttribute("flafForCheck", flag);
			model.addAttribute("tags", tags);
			model.addAttribute("newsVO", news);
			model.addAttribute("currentMenu", "add-news-nav");
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "editNews";
	}

	/**
	 * Save new news
	 * 
	 * @param newsVO
	 *            the news value object object
	 * @param tagIds
	 *            the list of tag's ids
	 * @param model
	 *            the model object
	 * @param result
	 *            the binding result model
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/saveNews", method = RequestMethod.POST)
	public String saveNews(@ModelAttribute("newsVO") NewsValueObject newsVO,
			@RequestParam(value = "selectedTags", required = false) Long[] tagIds, Model model, BindingResult result) {
		Long newsId = 0L;
		validator.validate(newsVO, result);
		try {
			if (result.hasErrors()) {
				List<Author> authors = newsManagementService.findAllAuthors();
				List<Tag> tags = newsManagementService.findAllTags();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				model.addAttribute("date", dateFormat.format(date));
				model.addAttribute("tags", tags);
				model.addAttribute("authors", authors);
				model.addAttribute("newsVO", newsVO);
				model.addAttribute("currentMenu", "add-news-nav");
				return "addNews";
			}
			newsVO.getNews().setModificationDate(new Date());
			if (tagIds != null) {
				newsVO.setTags(tagIds);
			}
			newsVO.getNews().setCreationTime(new Timestamp(new java.util.Date().getTime()));
			NewsValueObject news = newsManagementService.saveNews(newsVO.getNews(), newsVO.getAuthor(),
					newsVO.getTags());
			newsId = news.getNews().getNewsId();
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/viewNews/" + newsId;
	}

	/**
	 * Update news
	 * 
	 * @param newsVO
	 *            the news value object object
	 * @param tagIds
	 *            the list of tag's ids
	 * @param model
	 *            the model object
	 * @param result
	 *            the binding result object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/saveEditNews", method = RequestMethod.POST)
	public String saveEditNews(@ModelAttribute("newsVO") NewsValueObject newsVO,
			@RequestParam(value = "selectedTags", required = false) Long[] tagIds, Model model, BindingResult result) {
		Long newsId = 0L;
		validator.validate(newsVO, result);
		try {
			if (result.hasErrors()) {
				List<Author> authors = newsManagementService.findAllAuthors();
				List<Tag> tags = newsManagementService.findAllTags();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				model.addAttribute("date", dateFormat.format(new Date()));
				model.addAttribute("authors", authors);
				boolean flag = false;
				model.addAttribute("flafForCheck", flag);
				model.addAttribute("tags", tags);
				model.addAttribute("newsVO", newsVO);
				model.addAttribute("currentMenu", "add-news-nav");
				return "editNews";
			}
			newsVO.getNews().setModificationDate(new Date());
			newsVO.setAuthor(newsManagementService.findAuthorById(newsVO.getAuthor().getAuthorId()));
			List<Tag> list = new ArrayList<>();
			for (Long tagId : tagIds) {
				list.add(newsManagementService.findTagById(tagId));
			}
			newsVO.setTags(list);
			NewsValueObject news = newsManagementService.updateNews(newsVO);
			newsId = news.getNews().getNewsId();
		} catch (ServiceException e) {
			model.addAttribute("error", e);
			return "errorPage";
		}
		return "redirect:/viewNews/" + newsId;
	}

	/**
	 * Delete list of news
	 * 
	 * @param newsIdForDelete
	 *            the list of news's ids
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
	public String deleteNews(@RequestParam(value = "selectedNews", required = false)Long[] newsIdForDelete) {
		try {
			if ((newsIdForDelete != null) && (newsIdForDelete.length > 0)) {
				newsManagementService.deleteNews(newsIdForDelete);
			}
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "redirect:/news/pages/1";
	}

	/**
	 * Find news, which is next to the current news
	 * 
	 * @param currentNewsId
	 *            the news's id
	 * @param model
	 *            the model object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/nextNews/{currentNewsId}", method = RequestMethod.GET)
	public String nextNews(@PathVariable long currentNewsId, Model model, HttpSession session) {
		try {
			SearchCriteriaObject sc = (SearchCriteriaObject) session.getAttribute("searchCriteria");
			Long nextNewsId = newsManagementService.findNextNews(currentNewsId,sc);
			if (nextNewsId != -1) {
				currentNewsId = nextNewsId;
			}
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "redirect:/viewNews/" + currentNewsId;
	}

	/**
	 * Find news, which is previous to the current news
	 * 
	 * @param currentNewsId
	 *            the news's id
	 * @param model
	 *            the model object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/previousNews/{currentNewsId}", method = RequestMethod.GET)
	public String previousNews(Model model, @PathVariable long currentNewsId, HttpSession session) {
		Long prevNewsId = 0L;
		try {
			SearchCriteriaObject sc = (SearchCriteriaObject) session.getAttribute("searchCriteria");
			prevNewsId = newsManagementService.findPreviousNews(currentNewsId,sc);
			if (prevNewsId != -1) {
				currentNewsId = prevNewsId;
			}
		} catch (ServiceException e) {
			return "errorPage";
		}
		return "redirect:/viewNews/" + currentNewsId;
	}

	/**
	 * Reset filter of search criteria
	 * 
	 * @param session
	 *            the session object
	 * @return the string of redirecting url
	 */
	@RequestMapping(value = "/resetFilter", method = RequestMethod.GET)
	public String resetFilter(HttpSession session) {
		session.setAttribute("searchCriteria", null);
		return "redirect:/news";
	}

	private SearchCriteriaObject createSearchCriteria(Long authorId, List<Long> tagIds) {
		SearchCriteriaObject sc = new SearchCriteriaObject();
		if (authorId != null) {
			sc.setAuthorId(authorId);
		}
		if (tagIds != null) {
			sc.getTagIds(tagIds);
		}
		return sc;
	}
}
