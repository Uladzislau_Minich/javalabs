package com.epam.newsadmin.controller;


import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Class LoginController
 * 
 * @author Uladzislau
 *
 */
@Controller
public class LoginController {
	static {
		Locale.setDefault(Locale.ENGLISH);
	}
	/**
	 * This method sets all model attributes and returns login page view
	 * 
	 * @param error
	 *            sets true if login or password are incorrect
	 * @return login page view
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, HttpSession session, @RequestParam(value = "error", required = false) String error) {
		if (error != null) {
			model.addAttribute("error", "error");
		}
		return "login";
	}

	@RequestMapping(value = "/logout")
	public String login(Model model, HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}

}