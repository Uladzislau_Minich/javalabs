package com.epam.newscommon.service.impl;

import java.util.List;

import com.epam.newscommon.exception.DAOException;
import com.epam.newscommon.exception.ServiceException;
import com.epam.newscommon.dao.NewsDAO;
import com.epam.newscommon.entity.News;
import com.epam.newscommon.service.NewsService;
import com.epam.newscommon.utils.SearchCriteriaObject;

/**
 * Class NewsServiceImpl. Implementation of the NewsService
 * 
 * @author Uladzislau_Minich
 *
 */
public class NewsServiceImpl implements NewsService {
	private NewsDAO newsDAO;

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#save()}
	 */
	@Override
	public Long save(News entity) throws ServiceException {
		try {
			entity.setNewsId(newsDAO.create(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity.getNewsId();
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findById()}
	 */
	@Override
	public News findById(Long id) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.findById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#update()}
	 */
	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#deleteById(int)}
	 */
	@Override
	public void deleteById(Long[] ids) throws ServiceException {
		try {
			newsDAO.deleteById(ids);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}


	public List<News> findNewsPerPage(SearchCriteriaObject sc,int startIndex, int endIndex) throws ServiceException{
		List<News> newsAll = null;
		try {
			newsAll = newsDAO.findNewsPerPage(sc, startIndex, endIndex);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return newsAll;
	}
	/**
	 * @see {@link com.epam.newscommon.service.NewsService#saveNewsAuthors(int, int)}
	 */
	@Override
	public void addNewsAuthors(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.addNewsAuthors(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#deleteNewsAuthor(int)}
	 */
	@Override
	public void deleteNewsAuthor(Long[] newsIds) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthor(newsIds);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#addNewsTag(int, int)}
	 */
	@Override
	public void addNewsTag(Long tagId, Long newsId) throws ServiceException {
		try {
			newsDAO.addNewsTag(tagId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#deleteNewsTag(int)}
	 */
	@Override
	public void deleteNewsTag(Long[] idNews) throws ServiceException {
		try {
			newsDAO.deleteNewsTag(idNews);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findAllAuthorNews(int)}
	 */
	@Override
	public List<News> findAllAuthorNews(Long authorId) throws ServiceException {
		List<News> searchNews = null;
		try {
			searchNews = newsDAO.findAllAuthorsNews(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return searchNews;
	}
	/**
	 * @see {@link com.epam.newscommon.service.NewsService#countNews()}
	 */
	@Override
	public int countNews(SearchCriteriaObject sc) throws ServiceException {
		int amount = 0;
		try {
			 amount = newsDAO.countNews(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return amount;
	}

	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findPreviousNews(News)}
	 */
	@Override
	public Long findPreviousNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException {
		Long previousNewsId=0L;
		try {
			previousNewsId = newsDAO.findPreviousNews(currentNewsId,sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return previousNewsId;
	}
	/**
	 * @see {@link com.epam.newscommon.service.NewsService#findNextNews(News)}
	 */
	@Override
	public Long findNextNews(Long currentNewsId, SearchCriteriaObject sc) throws ServiceException {
		Long nextNewsId=0L;
		try {
			nextNewsId = newsDAO.findNextNews(currentNewsId,sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return nextNewsId;
	}
	
	/**
	 * Set  newsDAO
	 * 
	 * @param newsDAO
	 */
	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	


	

}
